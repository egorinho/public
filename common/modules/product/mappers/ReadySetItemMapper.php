<?php

namespace common\modules\product\mappers;

use common\modules\product\models\db\ReadySetItem;
use common\modules\product\models\dto\ReadySetItemDto;

/**
 * Class ReadySetItemMapper
 * @package common\modules\product\mappers
 */
class ReadySetItemMapper
{
    public static function map(ReadySetItem $item): ReadySetItemDto
    {
        $dto = new ReadySetItemDto();
        $dto->id = $item->id;
        $dto->height = $item->height;
        $dto->width = $item->width;
        $dto->productId = $item->product_id;
        $dto->productName = $item->product->site_name ?: $item->product->name;
        $dto->productSlug = $item->product->slug;
        $dto->rrc = $item->product->rrc;
        $dto->arc = $item->product->arc;

        return $dto;
    }

    /**
     * @param ReadySetItem[] $sets
     * @return ReadySetItemDto[]
     */
    public static function mapAll(array $sets): array
    {
        return array_map([self::class, 'map'], $sets);
    }
}
