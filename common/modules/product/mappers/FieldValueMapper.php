<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\FieldValue;
use common\modules\product\models\dto\FieldValueDto;

/**
 * Class FieldValueMapper
 * @package common\modules\product\mappers
 */
class FieldValueMapper
{
    public static function mapByFieldValuesAll(array $fields): array
    {
        return array_map([self::class, 'mapByFieldValue'], $fields);
    }

    public static function mapByFieldValue(FieldValue $fieldValue): FieldValueDto
    {
        $dto = new FieldValueDto();
        $dto->name = $fieldValue->field->name;
        $dto->value = $fieldValue->value;

        return $dto;
    }
}
