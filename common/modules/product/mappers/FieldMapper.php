<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\Field;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\dto\FieldDto;
use Exception;

/**
 * Class FieldMapper
 * @package common\modules\product\mappers
 */
class FieldMapper
{
    /**
     * @param FieldValue[] $values
     * @return FieldDto
     * @throws
     */
    public static function mapByFieldValueArray(array $values): FieldDto
    {
        if (empty($values)) {
            throw new Exception('Null array');
        }

        $dto = new FieldDto();
        $dto->slug = current($values)->field->slug;
        $dto->name = current($values)->field->name;
        $dto->hint = current($values)->field->hint;
        $dto->type_filter = current($values)->field->type_filter;
        $dto->values = [];

        foreach ($values as $value) {
            $dto->values[$value->value] = ['value' => $value->value, 'hint' => $value->hint];
        }

        return $dto;
    }

    /**
     * @param Field $field
     * @return FieldDto
     */
    public static function mapByField(Field $field): FieldDto
    {
        $dto = new FieldDto();
        $dto->slug = $field->slug;
        $dto->name = $field->name;

        foreach ($field->fieldValues as $value) {
            $dto->values[] = $value->value;
        }

        return $dto;
    }

    /**
     * @param Field[] $fields
     * @return FieldDto[]
     */
    public static function mapByFieldsAll(array $fields): array
    {
        return array_map([self::class, 'mapByField'], $fields);
    }
}
