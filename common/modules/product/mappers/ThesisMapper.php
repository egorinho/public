<?php

namespace common\modules\product\mappers;

use common\helpers\FileHelper;
use common\modules\product\models\db\Thesis;

/**
 * Class ThesisMapper
 * @package common\modules\product\mappers
 */
class ThesisMapper
{
    /**
     * @param Thesis $thesis
     * @return array
     */
    public static function map(Thesis $thesis): array
    {
        return [
            'name' => $thesis->name,
            'img' => FileHelper::getFileUrl($thesis->img),
            'text' => $thesis->text,
            'order' => $thesis->order,
        ];
    }

    /**
     * @param Thesis[] $theses
     * @return array
     */
    public static function mapAll(array $theses): array
    {
        return array_map([self::class, 'map'], $theses);
    }
}
