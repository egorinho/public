<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\CategoryArticleLink;
use common\modules\product\models\dto\CategoryArticleLinkDto;

/**
 * Class CategoryArticleLinkMapper
 * @package common\modules\product\mappers
 */
class CategoryArticleLinkMapper
{
    /**
     * @param CategoryArticleLink $articleLink
     * @return CategoryArticleLinkDto
     */
    public static function tile(CategoryArticleLink $articleLink): CategoryArticleLinkDto
    {
        $dto = new CategoryArticleLinkDto();
        $dto->name = $articleLink->name;
        $dto->link = $articleLink->link;
        $dto->image = $articleLink->getImgWebPath();

        return $dto;
    }
}
