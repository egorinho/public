<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\Category;
use common\modules\product\models\dto\CategoryTileDto;
use common\modules\product\models\dto\CategoryTileInGroupDto;

/**
 * Class CategoryMapper
 * @package common\modules\product\mappers
 */
class CategoryMapper
{
    /**
     * @param Category $category
     * @return CategoryTileDto
     */
    public static function tile(Category $category): CategoryTileDto
    {
        $tile = new CategoryTileDto();
        $tile->name = $category->name;
        $tile->slug = $category->slug;
        $tile->productQuantity = $category->product_count ?: $category->getProductCount();

        return $tile;
    }

    /**
     * @param Category[] $categories
     * @return CategoryTileDto[]
     */
    public static function tiles(array $categories): array
    {
        return array_map([self::class, 'tile'], $categories);
    }

    /**
     * @param Category $category
     * @return CategoryTileInGroupDto
     */
    public static function tileInGroup(Category $category): CategoryTileInGroupDto
    {
        $tile = new CategoryTileInGroupDto();
        $tile->name = $category->name;
        $tile->slug = $category->slug;
        $tile->img = $category->getImgWebPath();

        return $tile;
    }

    /**
     * @param Category[] $categories
     * @return CategoryTileInGroupDto[]
     */
    public static function tilesInGroup(array $categories): array
    {
        return array_map([self::class, 'tileInGroup'], $categories);
    }

    /**
     * @param Category $category
     * @return CategoryTileDto
     */
    public static function mapWithParent(Category $category): CategoryTileDto
    {
        $tile = new CategoryTileDto();
        $tile->id = $category->id;
        $tile->name = $category->name;
        $tile->slug = $category->slug;
        $tile->content = $category->content;
        $tile->parent = !empty($category->parent) ? self::mapWithParent($category->parent) : null;
        $tile->articleLink = $category->articleLink ? CategoryArticleLinkMapper::tile($category->articleLink) : null;

        return $tile;
    }
}
