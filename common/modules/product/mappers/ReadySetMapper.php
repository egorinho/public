<?php

namespace common\modules\product\mappers;

use common\modules\product\models\db\ReadySet;
use common\modules\product\models\dto\ReadySetDto;

/**
 * Class ReadySetMapper
 * @package common\modules\product\mappers
 */
class ReadySetMapper
{
    /**
     * @param ReadySet $set
     * @return ReadySetDto
     */
    public static function map(ReadySet $set): ReadySetDto
    {
        $dto = new ReadySetDto();
        $dto->id = $set->id;
        $dto->name = $set->name;
        $dto->slug = $set->slug;
        $dto->link = $set->link;
        $dto->description = $set->description;
        $dto->photo = $set->getImgUrl();
        $dto->collection = $set->collection ? CollectionMapper::tile($set->collection) : null;
        $dto->items = ReadySetItemMapper::mapAll($set->readySetItems);

        return $dto;
    }

    /**
     * @param ReadySet[] $sets
     * @return ReadySetDto[]
     */
    public static function mapAll(array $sets): array
    {
        return array_map([self::class, 'map'], $sets);
    }
}
