<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\ProductGroup;
use common\modules\product\models\dto\ProductGroupDto;

/**
 * Class ProductGroupMapper
 * @package common\modules\product\mappers
 */
class ProductGroupMapper
{
    /**
     * @param ProductGroup $group
     * @return ProductGroupDto
     */
    public static function tile(ProductGroup $group): ProductGroupDto
    {
        $tile = new ProductGroupDto();
        $tile->name = $group->name;
        $tile->slug = $group->slug;
        $tile->id = $group->id;

        return $tile;
    }
}
