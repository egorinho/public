<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\Document;
use common\modules\product\models\db\Photo;
use common\modules\product\models\db\Product;
use common\modules\product\models\dto\ProductDto;

/**
 * Class ProductMapper
 * @package common\modules\product\mappers
 */
class ProductMapper
{
    /**
     * @param Product $product
     * @return ProductDto
     */
    public static function map(Product $product): ProductDto
    {
        $dto = static::mapForList($product);
        $dto->commonDescription = $product->common_description;
        $dto->uniqDescription = $product->uniq_description;
        $dto->color = $product->colorModel;
        $dto->documents = array_map(function (Document $document) {
            return $document->getUrl();
        }, $product->documents);
        $dto->fields = FieldValueMapper::mapByFieldValuesAll($product->fieldValues);

        $dto->pickings = PickingMapper::mapAll($product->getPickingsCategory());
        return $dto;
    }

    /**
     * @param Product $product
     * @return ProductDto
     */
    public static function mapWithColorRelations(Product $product): ProductDto
    {
        $dto = new ProductDto();
        $dto->id = $product->id;
        $dto->name = $product->name;
        $dto->slug = $product->slug;
        $dto->article = $product->article;
        $dto->rrc = $product->rrc;
        $dto->arc = $product->arc;
        $dto->photoUrl = $product->getMainPhotoUrl();
        $dto->siteName = $product->site_name;
        $dto->commonDescription = $product->common_description;
        $dto->uniqDescription = $product->uniq_description;
        $dto->color = $product->colorModel ? ProductColorMapper::map($product->colorModel) : [];
        $dto->category = CategoryMapper::tile($product->category);
        $dto->collection = CollectionMapper::tile($product->collection);
        $dto->photos = array_map(function (Photo $photo) {
            return $photo->getImgUrl();
        }, $product->photos);

        return $dto;
    }

    /**
     * @param Product[] $products
     * @return ProductDto[]
     */
    public static function mapAll(array $products): array
    {
        return array_map(function (Product $product) {
            return self::mapForList($product);
        }, $products);
    }


    public static function mapForList(Product $product): ProductDto
    {
        $dto = new ProductDto();
        $dto->id = $product->id;
        $dto->name = $product->name;
        $dto->slug = $product->slug;
        $dto->article = $product->article;
        $dto->isStock = $product->quantity > 0;
        $dto->rrc = $product->rrc;
        $dto->arc = $product->arc;
        $dto->color = $product->colorModel ? ProductColorMapper::map($product->colorModel) : [];
        $dto->category = CategoryMapper::tile($product->category);
        $dto->collection = CollectionMapper::tile($product->collection);
        $dto->photos = array_map(function (Photo $photo) {
            return $photo->getImgUrl();
        }, $product->photos);
        $dto->tags = TagMapper::mapAll($product->tags);
        $dto->fields = FieldValueMapper::mapByFieldValuesAll($product->mainFieldsValues);

        if (!empty($product->arc) && !empty($product->rrc)) {
            $dto->discount = (int)((1 - $product->arc / $product->rrc) * 100) . '%';
        }

        $dto->sameWithDifferentColor = array_map(
            [self::class, 'mapWithColorRelations'],
            $product->sameWithDifferentColor
        );

        return $dto;
    }
}
