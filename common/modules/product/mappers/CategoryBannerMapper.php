<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\modules\product\models\db\CategoryBanner;
use common\modules\product\models\dto\CategoryBannerDto;

/**
 * Class CategoryBannerMapper
 * @package common\modules\product\mappers
 */
class CategoryBannerMapper
{
    /**
     * @param CategoryBanner $banner
     * @return CategoryBannerDto
     */
    public static function map(CategoryBanner $banner): CategoryBannerDto
    {
        $dto = new CategoryBannerDto();
        $dto->title = $banner->title;
        $dto->content = $banner->content;
        $dto->image = $banner->getImgWebPath();

        return $dto;
    }
}
