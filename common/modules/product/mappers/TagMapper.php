<?php


namespace common\modules\product\mappers;


use common\modules\product\models\db\Tag;
use common\modules\product\models\dto\TagDto;

class TagMapper
{
    public static function map(Tag $tag): TagDto
    {
        $dto = new TagDto();
        $dto->id = $tag->id;
        $dto->name = $tag->name;

        return $dto;
    }

    public static function mapAll(array $tags): array
    {
        return array_map(function (Tag $tag) {
            return self::map($tag);
        }, $tags);
    }
}
