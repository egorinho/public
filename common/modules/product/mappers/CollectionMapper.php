<?php

declare(strict_types=1);

namespace common\modules\product\mappers;

use common\helpers\FileHelper;
use common\modules\product\models\db\Collection;
use common\modules\product\models\dto\CollectionDto;
use common\modules\product\models\dto\CollectionTileDto;

/**
 * Class CollectionMapper
 * @package common\modules\product\mappers
 */
class CollectionMapper
{
    /**
     * @param Collection $collection
     * @return CollectionTileDto
     */
    public static function tile(Collection $collection): CollectionTileDto
    {
        $dto = new CollectionTileDto();
        $dto->id = $collection->id;
        $dto->name = $collection->name;
        $dto->slug = $collection->slug;
        $dto->productQuantity = $collection->product_count ?: count($collection->products);

        return $dto;
    }

    /**
     * @param Collection[] $collections
     * @return CollectionTileDto[]
     */
    public static function tiles(array $collections): array
    {
        return array_map([self::class, 'tile'], $collections);
    }

    public static function mapWithImg(Collection $collection): CollectionTileDto
    {
        $dto = new CollectionTileDto();
        $dto->id = $collection->id;
        $dto->name = $collection->name;
        $dto->slug = $collection->slug;
        $dto->description_preview = $collection->description_preview;
        $dto->img = FileHelper::getFileUrl($collection->img);

        return $dto;
    }

    /**
     * @param Collection[] $collections
     * @return CollectionTileDto[]
     */
    public static function mapWithImgAll(array $collections): array
    {
        return array_map([self::class, 'mapWithImg'], $collections);
    }

    /**
     * @param Collection $collection
     * @return CollectionDto
     */
    public static function map(Collection $collection): CollectionDto
    {
        $dto = new CollectionDto();
        $dto->name = $collection->name;
        $dto->slug = $collection->slug;
        $dto->img = FileHelper::getFileUrl($collection->img);
        $dto->descriptionPreview = $collection->description_preview;
        $dto->descriptionAll = $collection->description_all;
        $dto->products = ProductMapper::mapAll($collection->products);

        return $dto;
    }

    /**
     * @param Collection[] $collections
     * @return CollectionDto[]
     */
    public static function mapAll(array $collections): array
    {
        return array_map([self::class, 'map'], $collections);
    }
}
