<?php

namespace common\modules\product\validators;

use common\modules\product\models\db\Color;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class ColorValidator
 * @package common\modules\product\validators
 */
class ColorValidator extends Validator
{
    /**
     * Validates a single attribute.
     * Child classes must implement this method to provide the actual validation logic.
     * @param Model $model the data model to be validated
     * @param string $attribute the name of the attribute to be validated.
     */
    public function validateAttribute($model, $attribute)
    {
        $count = Color::find()->where(['slug' => $model->$attribute])->count();
        $model->$attribute = explode(',', $model->$attribute);

        if ($count !== count($model->$attribute)) {
            foreach ($model->$attribute as $slug) {
                $color = Color::findOne(['slug' => $slug]);

                if (empty($color)) {
                    $this->addError($model, $attribute, 'Не найден цвет: ' . $slug);
                }
            }
        }
    }
}
