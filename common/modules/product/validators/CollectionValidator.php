<?php

namespace common\modules\product\validators;

use common\modules\product\models\db\Collection;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class CollectionValidator
 * @package common\modules\product\validators
 */
class CollectionValidator extends Validator
{
    /**
     * Validates a single attribute.
     * Child classes must implement this method to provide the actual validation logic.
     * @param Model $model the data model to be validated
     * @param string $attribute the name of the attribute to be validated.
     */
    public function validateAttribute($model, $attribute)
    {
        $count = Collection::find()->where(['slug' => $model->$attribute])->count();
        $model->$attribute = explode(',', $model->$attribute);

        if ($count !== count($model->$attribute)) {
            foreach ($model->$attribute as $slug) {
                $collection = Collection::findOne(['slug' => $slug]);

                if (empty($collection)) {
                    $this->addError($model, $attribute, 'Не найдена коллекция: ' . $slug);
                }
            }
        }
    }
}
