<?php

declare(strict_types=1);

namespace common\modules\product\services;

use common\modules\product\mappers\CategoryMapper;
use common\modules\product\mappers\ProductGroupMapper;
use common\modules\product\models\CatalogParams;
use common\modules\product\models\dto\CatalogDataDto;
use common\modules\product\models\dto\CategoryTileDto;
use common\modules\product\models\dto\CollectionTileDto;
use common\modules\product\models\dto\ColorTileDto;
use common\modules\product\models\dto\FieldDto;
use common\modules\product\models\dto\PriceFilterDto;
use common\modules\product\models\dto\ProductDto;
use common\modules\product\models\search\CategoryFilterSearch;
use common\modules\product\models\search\CollectionFilterSearch;
use common\modules\product\models\search\ColorFilterSearch;
use common\modules\product\models\search\FieldFilterSearch;
use common\modules\product\models\search\FieldValueFilterSearch;
use common\modules\product\models\search\PriceFilterSearch;
use common\modules\product\models\search\ProductQueryCreator;
use common\modules\product\models\search\ProductSearch;
use common\modules\product\repositories\CategoryRepository;
use common\modules\product\repositories\CollectionRepository;
use common\modules\product\repositories\ProductGroupRepository;

/**
 * Class CatalogService
 * @package common\modules\product\services
 */
class CatalogService
{
    private CategoryRepository $categoryRepository;
    private CollectionRepository $collectionRepository;
    private ProductGroupRepository $productGroupRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        CollectionRepository $collectionRepository,
        ProductGroupRepository $productGroupRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->collectionRepository = $collectionRepository;
        $this->productGroupRepository = $productGroupRepository;
    }

    /**
     * @param array $params
     * @return CatalogDataDto
     * @throws
     */
    public function getData(array $params): CatalogDataDto
    {
        $catalogParams = new CatalogParams($params);
        $queryCreator = new ProductQueryCreator($catalogParams);

        $result = new CatalogDataDto();
        $result->priceFilters = $this->getPriceFilters($queryCreator);
        $result->categoryFilters = $this->getCategoryFilters($queryCreator);
        $result->collectionFilters = $this->getCollectionFilters($queryCreator);
        $result->colorFilters = $this->getColorFilters($queryCreator);
        $result->fieldFilters = $this->getFieldFilters($queryCreator, $catalogParams);
        $result->products = $this->getProducts($queryCreator, $catalogParams);
        $result->recommendedCollections = $this->collectionRepository->findAllForCatalog();

        $this->setCategory($result, $queryCreator, $catalogParams);
        $this->setGroup($result, $queryCreator, $catalogParams);
        $this->setGroupCategories($result, $catalogParams);

        return $result;
    }

    /**
     * @param array $params
     * @return int
     */
    public function getProductCount(array $params): int
    {
        $catalogParams = new CatalogParams($params);
        $queryCreator = new ProductQueryCreator($catalogParams);

        return $queryCreator->createQuery()->count();
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @param CatalogParams $params
     * @return ProductDto[]
     */
    private function getProducts(ProductQueryCreator $queryCreator, CatalogParams $params): array
    {
        $query = $queryCreator->createQuery();
        $searchModel = new ProductSearch($query, $params);

        return $searchModel->search();
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @return PriceFilterDto
     */
    private function getPriceFilters(ProductQueryCreator $queryCreator): PriceFilterDto
    {
        $query = $queryCreator->createQueryWithoutPrice();
        $searchModel = new PriceFilterSearch($query);

        return $searchModel->search();
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @return CategoryTileDto[]
     */
    private function getCategoryFilters(ProductQueryCreator $queryCreator): array
    {
        $query = $queryCreator->createQuery();
        $searchModel = new CategoryFilterSearch($query);

        return $searchModel->search($queryCreator->getCategorySlug());
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @param CatalogParams $params
     * @return FieldDto[]
     */
    private function getFieldFilters(ProductQueryCreator $queryCreator, CatalogParams $params): array
    {
        $filters = [];

        foreach ($params->getOtherParams() as $field => $value) {
            $query = $queryCreator->createQueryWithoutFieldParam($field);
            $searchModel = new FieldValueFilterSearch($query, $field);
            $filters[] = $searchModel->search();
        }

        $query = $queryCreator->createQuery();
        $searchModel = new FieldFilterSearch($query, $params->getOtherParams());
        $other = $searchModel->search();

        return array_merge($filters, $other);
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @return CollectionTileDto[]
     */
    private function getCollectionFilters(ProductQueryCreator $queryCreator): array
    {
        $query = $queryCreator->createQueryWithoutCollection();
        $searchModel = new CollectionFilterSearch($query);

        return $searchModel->search();
    }

    /**
     * @param ProductQueryCreator $queryCreator
     * @return ColorTileDto[]
     */
    private function getColorFilters(ProductQueryCreator $queryCreator): array
    {
        $query = $queryCreator->createQueryWithoutColor();
        $searchModel = new ColorFilterSearch($query);

        return $searchModel->search();
    }

    /**
     * @param CatalogDataDto $result
     * @param ProductQueryCreator $queryCreator
     * @param $catalogParams
     */
    private function setCategory(
        CatalogDataDto $result,
        ProductQueryCreator $queryCreator,
        CatalogParams $catalogParams
    ): void {
        if (!empty($catalogParams->category)) {
            $result->category = CategoryMapper::mapWithParent(
                $this->categoryRepository->findBySlug($catalogParams->category)
            );
            $result->category->productQuantity = $queryCreator->createQuery()->count();
        }
    }

    /**
     * @param CatalogDataDto $result
     * @param ProductQueryCreator $queryCreator
     * @param $catalogParams
     */
    private function setGroup(
        CatalogDataDto $result,
        ProductQueryCreator $queryCreator,
        CatalogParams $catalogParams
    ): void {
        if (!empty($catalogParams->group) && empty($catalogParams->category)) {
            $result->group = ProductGroupMapper::tile($this->productGroupRepository->findBySlug($catalogParams->group));
            $result->group->productQuantity = $queryCreator->createQuery()->count();
        }
    }

    /**
     * @param CatalogDataDto $result
     * @param CatalogParams $params
     */
    private function setGroupCategories(CatalogDataDto $result, CatalogParams $params): void
    {
        if (!empty($params->group)) {
            $result->groupCategories = CategoryMapper::tilesInGroup(
                $this->categoryRepository->findAllByGroup($params->group)
            );
        }
    }
}
