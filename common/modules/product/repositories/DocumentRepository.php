<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Document;

/**
 * Class DocumentRepository
 * @package common\modules\product\repositories
 */
class DocumentRepository
{
    public function findAllForProductUrls(): array
    {
        return Document::find()
            ->select('file_url')
            ->where(['not', ['model_id' => null]])
            ->andWhere(['model' => Document::MODEL_PRODUCT])
            ->column();
    }
}
