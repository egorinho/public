<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Thesis;

/**
 * Class ThesisRepository
 * @package common\modules\product\repositories
 */
class ThesisRepository
{
    /**
     * @return array
     */
    public function findAllTypeHome(): array
    {
        return Thesis::findAll(['type' => Thesis::TYPE_HOME]);
    }

    public function findAllTypeCatalogByIds(array $thesisIds, int $limit = 0): array
    {
        $query = Thesis::find()
            ->andWhere(['in', 'id', $thesisIds])
            ->andWhere(['type' => Thesis::TYPE_CATALOG]);

        if (empty($limit)) {
            $query->limit($limit);
        }
        return $query->all();
    }

}
