<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\ProductGroup;

/**
 * Class ProductGroupRepository
 * @package common\modules\product\repositories
 */
class ProductGroupRepository
{
    public function findAll(): array
    {
        return ProductGroup::find()
            ->indexBy('slug')
            ->all();
    }

    /**
     * @param string $slug
     * @return ProductGroup|null
     */
    public function findBySlug(string $slug): ?ProductGroup
    {
        return ProductGroup::findOne(['slug' => $slug]);
    }

    /**
     * @return string[]
     */
    public function getSlugs(): array
    {
        return ProductGroup::find()->select('slug')->column();
    }
}