<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Category;
use common\modules\product\models\db\CategoryBuyTogetherRelation;
use common\modules\product\models\db\Product;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProductRepository
 * @package common\modules\product\repositories
 */
class ProductRepository
{
    /**
     * @return Product[]
     */
    public function findAllIndexByArticle(): array
    {
        return Product::find()
            ->indexBy('article')
            ->all();
    }

    /**
     * @return Product[]
     */
    public function findAllByCollectionIdLimited($collection_id): array
    {
        return Product::find()
            ->andWhere(['collection_id' => $collection_id])
            ->limit(15)
            ->all();
    }

    /**
     * @return int[]
     */
    public function findAllIdsIndexByArticle(): array
    {
        return Product::find()
            ->select('id')
            ->indexBy('article')
            ->column();
    }

    /**
     * @return Product[]
     */
    public function findAllIndexByArticleWithApiPhotos(): array
    {
        return Product::find()
            ->with('apiPhotos')
            ->indexBy('article')
            ->all();
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public function findMinPriceByCategoryId(int $categoryId): ?float
    {
        return Product::find()
            ->select(['MIN(COALESCE(NULLIF(arc, 0),rrc))'])
            ->where(['category_id' => $categoryId])
            ->scalar();
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public function findMaxPriceByCategoryId(int $categoryId): ?float
    {
        return Product::find()
            ->select(['MAX(COALESCE(NULLIF(arc, 0), rrc))'])
            ->where(['category_id' => $categoryId])
            ->scalar();
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public function findMinPriceByParentCategoryId(int $categoryId): ?float
    {
        return Product::find()
            ->select(['MIN(COALESCE(NULLIF(arc, 0),rrc))'])
            ->where(['category_id' => Category::find()->where(['parent_id' => $categoryId])->column()])
            ->scalar();
    }

    /**
     * @return float
     */
    public function findMaxPrice(): ?float
    {
        return Product::find()
            ->select(['MAX(COALESCE(NULLIF(arc, 0), rrc))'])
            ->scalar();
    }

    /**
     * @return float
     */
    public function findMinPrice(): ?float
    {
        return Product::find()
            ->select(['MIN(COALESCE(NULLIF(arc, 0),rrc))'])
            ->scalar();
    }

    /**
     * @param int $categoryId
     * @return float
     */
    public function findMaxPriceByParentCategoryId(int $categoryId): ?float
    {
        return Product::find()
            ->select(['MAX(COALESCE(NULLIF(arc, 0), rrc))'])
            ->where(['category_id' => Category::find()->where(['parent_id' => $categoryId])->column()])
            ->scalar();
    }

    /**
     * @param string $slug
     * @return Product|null
     */
    public function findOneBySlug(string $slug): ?Product
    {
        return Product::findOne(['slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return Product
     * @throws
     */
    public function getOneBySlug(string $slug): Product
    {
        $product = $this->findOneBySlug($slug);

        if (!$product) {
            throw new Exception('Product not found');
        }

        return $product;
    }

    /**
     * @param string $slug
     * @return Product[]
     */
    public function getAllSalesLeadersByCategorySlug(string $slug): array
    {
        return Product::find()
            ->andWhere(['category_id' => Category::getQueryChildrenByParams('slug', $slug)])
            ->andWhere(['is_sales_leaders' => true])
            ->limit(15)
            ->orderBy(new Expression('RANDOM ()'))
            ->all();
    }

    /**
     * @return ActiveRecord|null
     */
    public function getOneSalesLeaders(): ?ActiveRecord
    {
        return Product::find()
            ->andWhere(['is_sales_leaders' => true])
            ->orderBy(new Expression('RANDOM ()'))
            ->limit(1)
            ->one();
    }

    /**
     * @param $price
     * @return array
     */
    public function getAllSameProductByPriceLimited($price): array
    {
        return Product::find()
            ->andWhere(['>=', 'rrc', ($price * (1 - Product::PERCENT_PRICE_FOR_SAME_PRODUCT / 100))])
            ->andWhere(['<=', 'rrc', ($price * (1 + Product::PERCENT_PRICE_FOR_SAME_PRODUCT / 100))])
            ->limit(10)
            ->all();
    }

    /**
     * @return Product[]
     */
    public function findAllSalesLeaderForHome(): array
    {
        return Product::find()
            ->with('category')
            ->where(['product.is_sales_leaders' => true, 'category.show_leaders_home' => true])
            ->all();
    }

    /**
     * @param int $category_id
     * @return array
     */
    public function getAllBuyTogetherProductsLimited(int $category_id): array
    {
        return Product::find()
            ->andWhere([
                'in',
                'category_id',
                CategoryBuyTogetherRelation::find()
                    ->select('category_two_id')
                    ->andWhere(['category_one_id' => $category_id])
            ])
            ->orderBy(new Expression('RANDOM ()'))
            ->limit(15)
            ->all();
    }

    /**
     * @param Product $product
     * @throws Exception
     */
    public function save(Product $product): void
    {
        if (!$product->save()) {
            throw new Exception('Product dont save');
        }
    }
}
