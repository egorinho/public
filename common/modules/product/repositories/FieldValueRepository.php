<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Category;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\db\Product;

/**
 * Class FieldValueRepository
 * @package common\modules\product\repositories
 */
class FieldValueRepository
{
    /**
     * @param int $categoryId
     * @param int $fieldId
     * @return string[]
     */
    public function findValuesByCategoryIdAndFieldId(int $categoryId, int $fieldId): array
    {
        return FieldValue::find()
            ->select('field_value.value')
            ->innerJoin(Product::tableName(), 'product.id = field_value.product_id')
            ->where(['field_value.field_id' => $fieldId, 'product.category_id' => $categoryId])
            ->distinct()
            ->column();
    }

    /**
     * @param int $categoryId
     * @param int $fieldId
     * @return string[]
     */
    public function findValuesByParentCategoryIdAndFieldId(int $categoryId, int $fieldId): array
    {
        return FieldValue::find()
            ->select('field_value.value')
            ->innerJoin(Product::tableName(), 'product.id = field_value.product_id')
            ->where([
                'field_value.field_id' => $fieldId,
                'product.category_id' => Category::find()->where(['parent_id' => $categoryId])->column()
            ])
            ->distinct()
            ->column();
    }

    /**
     * @param int $fieldId
     * @return string[]
     */
    public function findValuesByFieldId(int $fieldId): array
    {
        return FieldValue::find()
            ->select('field_value.value')
            ->innerJoin(Product::tableName(), 'product.id = field_value.product_id')
            ->where(['field_value.field_id' => $fieldId,])
            ->distinct()
            ->column();
    }
}
