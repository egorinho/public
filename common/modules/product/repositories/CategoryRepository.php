<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Category;
use phpDocumentor\Reflection\Types\This;
use yii\base\BaseObject;
use yii\db\Exception;
use yii\db\Query;

/**
 * Class CategoryRepository
 * @package common\modules\product\repositories
 */
class CategoryRepository
{
    /**
     * @return Category[]
     */
    public function findChildAll(): array
    {
        return Category::find()
            ->andWhere(['not', ['parent_id' => null]])
            ->indexBy('name_1c')
            ->all();
    }

    /**
     * @return Category[]
     */
    public function findParentAll(): array
    {
        return Category::find()
            ->andWhere(['is', 'parent_id', null])
            ->indexBy('name_1c')
            ->all();
    }

    /**
     * @return Category[]
     */
    public function findAllWithoutParent(): array
    {
        return Category::findAll(['parent_id' => null]);
    }

    /**
     * @param string $slug
     * @return Category|null
     */
    public function findBySlug(string $slug): ?Category
    {
        return Category::findOne(['slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return array
     */
    public function findChildrenExceptThis(string $slug): array
    {
        return Category::find()
            ->where([
                'parent_id' => (new Query())
                    ->select('parent_id')
                    ->from(Category::tableName())
                    ->where(['slug' => $slug])
                    ->column()
            ])
            ->andWhere(['not', ['slug' => $slug]])
            ->all();
    }

    /**
     * @param string $slug
     * @return Category
     * @throws Exception
     */
    public function getBySlug(string $slug): Category
    {
        $category = $this->findBySlug($slug);

        if (empty($category)) {
            throw new Exception('Category not found');
        }

        return $category;
    }

    /**
     * @param string $group
     * @return array
     */
    public function findAllByGroup(string $group): array
    {
        return Category::findAll([$group => true]);
    }

    /**
     * @param string $paramName
     * @param string $paramValue
     * @return array
     */
    public function getAllThesisByParamNameAndValue(string $paramName, string $paramValue): array
    {
        $initialQuery = Category::find()
            ->select(['thesis', 'id', 'parent_id'])
            ->where([$paramName => $paramValue]);

        $recursiveQuery = Category::find()
            ->select(['category.thesis', 'category.id', 'category.parent_id'])
            ->innerJoin('r', 'r.parent_id = category.id');

        return (new Query())
            ->select(['thesis', 'id', 'parent_id'])
            ->from('r')
            ->withQuery($initialQuery->union($recursiveQuery), 'r', true)->column();
    }

    /**
     * @return Category[]
     */
    public function findAllShowLeaderHome(): array
    {
        return Category::find()->with('leadersProducts')->where(['show_leaders_home' => true])->all();
    }

    /**
     * @return Category[]
     */
    public function findAllShowProfitableHome(): array
    {
        return Category::find()->with('profitableProducts')->where(['show_profitable_home' => true])->all();
    }

    /**
     * @return Category[]
     */
    public function findAllShowNoveltyHome(): array
    {
        return Category::find()->with('noveltyProducts')->where(['show_novelty_home' => true])->all();
    }
}
