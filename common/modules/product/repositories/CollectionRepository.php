<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Category;
use common\modules\product\models\db\Collection;
use common\modules\product\models\db\Product;
use yii\db\Query;

/**
 * Class CollectionRepository
 * @package common\modules\product\repositories
 */
class CollectionRepository
{
    /**
     * @return Collection[]
     */
    public function findAll(): array
    {
        return Collection::find()
            ->indexBy('name')
            ->all();
    }

    /**
     * @param int $categoryId
     * @return Collection[]
     */
    public function findAllByCategoryId(int $categoryId): array
    {
        return Collection::find()
            ->where([
                'id' => (new Query())
                    ->select('collection_id')
                    ->from(Product::tableName())
                    ->where(['category_id' => $categoryId])
                    ->distinct()
                    ->column()
            ])
            ->all();
    }

    /**
     * @param int $categoryId
     * @return Collection[]
     */
    public function findAllByParentCategoryId(int $categoryId): array
    {
        return Collection::find()
            ->where([
                'id' => Product::find()
                    ->select('collection_id')
                    ->where(['category_id' => Category::find()->where(['parent_id' => $categoryId])->column()])
                    ->distinct()
                    ->column()
            ])
            ->all();
    }

    /**
     * @return array
     */
    public function findAllForCatalog(): array
    {
        return Collection::find()
            ->where(['catalog' => true])
            ->all();
    }

    /**
     * @param array $categoryIds
     * @return array
     */
    public function findAllByCategoryIds(array $categoryIds): array
    {
        return Product::find()
            ->select('collection.name as name,
            collection.slug as slug,category.root_category_id as root_category_id')
            ->distinct()
            ->leftJoin(Collection::tableName(),
                Collection::tableName() . '.id=' . Product::tableName() . '.collection_id')
            ->leftJoin(Category::tableName(),
                Category::tableName() . '.id=' . Product::tableName() . '.category_id')
            ->andWhere([
                'in',
                'category.id',
                Category::find()->select('cat.id')->alias('cat')->andWhere(['in', 'cat.root_category_id', $categoryIds])
            ])->asArray()->all();
    }
}
