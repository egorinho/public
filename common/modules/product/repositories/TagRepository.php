<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Tag;
use RuntimeException;

class TagRepository
{
    public function findAll(): array
    {
        return Tag::find()
            ->indexBy('name_1c')
            ->all();
    }

    public function save(Tag $record)
    {
        if (!$record->save()) {
            throw new RuntimeException('Ошибка сохранения');

        }
    }
}