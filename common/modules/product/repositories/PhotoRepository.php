<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Photo;

/**
 * Class PhotoRepository
 * @package common\modules\product\repositories
 */
class PhotoRepository
{
    public function findAllForProductUrls(): array
    {
        return Photo::find()
            ->select('file_url')
            ->where(['not', ['model_id' => null]])
            ->andWhere(['model' => Photo::MODEL_PRODUCT])
            ->column();
    }

    /**
     * @param int $productId
     * @return int|null
     */
    public function getMaxSortByProductId(int $productId): ?int
    {
        return Photo::find()
            ->select('MAX(sort)')
            ->where(['model_id' => $productId, 'model' => Photo::MODEL_PRODUCT])
            ->scalar();
    }
}
