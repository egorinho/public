<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\ReadySet;

/**
 * Class ReadySetRepository
 * @package common\modules\product\repositories
 */
class ReadySetRepository
{
    /**
     * @return ReadySet[]
     */
    public function findAllWithItems(): array
    {
        return ReadySet::find()
            ->with('readySetItems')
            ->all();
    }
}
