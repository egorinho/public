<?php

namespace common\modules\product\repositories;

use common\modules\product\models\db\Category;
use common\modules\product\models\db\Field;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\db\Product;
use RuntimeException;

/**
 * Class FieldRepository
 * @package common\modules\product\repositories
 */
class FieldRepository
{
    /**
     * @param int $categoryId
     * @return Field[]
     */
    public function findAllByCategoryIdWithValue(int $categoryId): array
    {
        return Field::find()
            ->innerJoin(FieldValue::tableName(), 'field_value.field_id = field.id')
            ->innerJoin(Product::tableName(), 'product.id = field_value.product_id')
            ->where(['product.category_id' => $categoryId, 'type' => Field::TYPE_ATTRIBUTE])
            ->all();
    }

    /**
     * @param int $categoryId
     * @return Field[]
     */
    public function findAllByParentCategoryIdWithValue(int $categoryId): array
    {
        return Field::find()
            ->innerJoin(FieldValue::tableName(), 'field_value.field_id = field.id')
            ->innerJoin(Product::tableName(), 'product.id = field_value.product_id')
            ->where([
                'product.category_id' => Category::find()->where(['parent_id' => $categoryId])->column(),
                'type' => Field::TYPE_ATTRIBUTE
            ])
            ->all();
    }

    /**
     * @return Field[]
     */
    public function findAll(): array
    {
        return Field::find()
            ->indexBy('name')
            ->all();
    }

    /**
     * @return Field[]
     */
    public function findAllTypeAttribute(): array
    {
        return Field::find()
            ->where(['type' => Field::TYPE_ATTRIBUTE])
            ->indexBy('name')
            ->all();
    }

    /**
     * @return Field[]
     */
    public function findAllTypeLogistic(): array
    {
        return Field::find()
            ->where(['type' => Field::TYPE_LOGISTIC])
            ->indexBy('name')
            ->all();
    }

    /**
     * @param Field $record
     * @return bool
     */
    public function save(Field $record): bool
    {
        if (!$record->save()) {
            throw new RuntimeException('Ошибка сохранения');
        }

        return true;
    }
}
