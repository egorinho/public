<?php

namespace common\modules\product\models\dto;

/**
 * Class CollectionDto
 * @package common\modules\product\models\dto
 */
class CollectionDto
{
    public string $name;
    public string $slug;
    public string $img;
    public string $descriptionPreview;
    public string $descriptionAll;
    /** @var ProductDto[] */
    public array $products;
}
