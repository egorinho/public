<?php

namespace common\modules\product\models\dto;

/**
 * Class FieldDto
 * @package common\modules\product\models\dto
 */
class FieldDto
{
    public string $slug;
    public string $name;
    public ?string $hint;
    public string $type_filter;
    /** @var string[] */
    public array $values;
}
