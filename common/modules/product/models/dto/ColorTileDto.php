<?php

namespace common\modules\product\models\dto;

/**
 * Class ColorTileDto
 * @package common\modules\product\models\dto
 */
class ColorTileDto
{
    public string $slug;
    public string $name;
    public ?string $rgb;
    public ?string $img;
    public int $productQuantity;
}
