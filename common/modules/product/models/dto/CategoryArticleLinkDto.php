<?php

namespace common\modules\product\models\dto;

/**
 * Class CategoryArticleLinkDto
 * @package common\modules\product\models\dto
 * @property string $name
 * @property string $link
 * @property string $image
 */
class CategoryArticleLinkDto
{
    public string $name;
    public string $link;
    public string $image;
}