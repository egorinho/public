<?php

namespace common\modules\product\models\dto;

/**
 * Class PriceFilterDto
 * @package common\modules\product\models\dto
 * @property $maxPrice
 * @property $minPrice
 */
class PriceFilterDto
{
    public $maxPrice;
    public $minPrice;

    public function __construct(?int $minPrice, ?int $maxPrice)
    {
        $this->minPrice = $minPrice ?: 0;
        $this->maxPrice = $maxPrice ?: 0;
    }
}
