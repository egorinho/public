<?php


namespace common\modules\product\models\dto;


class ProductColorDto
{
    public string $name;
    public string $slug;
    public ?string $rgb;
    public ?string $img;
}
