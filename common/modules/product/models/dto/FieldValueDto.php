<?php

namespace common\modules\product\models\dto;

/**
 * Class FieldValueDto
 * @package common\modules\product\models\dto
 * @property string $name
 * @property string $value
 */
class FieldValueDto
{
    public string $name;
    public string $value;
}