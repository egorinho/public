<?php

namespace common\modules\product\models\dto;

/**
 * Class PickingDto
 * @package common\modules\product\models\dto
 * @property string $name
 * @property string $image
 */
class PickingDto
{
    public string $name;
    public string $image;
}