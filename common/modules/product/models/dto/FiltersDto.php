<?php

namespace common\modules\product\models\dto;

use common\models\dto\ColorTileDto;

/**
 * Class FiltersDto
 * @package common\modules\product\models\dto
 */
class FiltersDto
{
    /** @var PriceFilterDto */
    public $price;
    /** @var CollectionTileDto[] */
    public $collections;
    /** @var ColorTileDto[] */
    public $colors;
    /** @var array */
    public $other;
}
