<?php

namespace common\modules\product\models\dto;

/**
 * Class CategoryTileInGroupDto
 * @package common\modules\product\models\dto
 */
class CategoryTileInGroupDto
{
    public string $name;
    public string $slug;
    public string $img;
}
