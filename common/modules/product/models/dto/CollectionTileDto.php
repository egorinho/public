<?php

namespace common\modules\product\models\dto;

/**
 * Class CollectionTileDto
 * @package common\modules\product\models\dto
 * @property string $name
 * @property string $slug
 * @property int $productQuantity
 */
class CollectionTileDto
{
    public $id;
    public $name;
    public $slug;
    public $productQuantity;
    public $img;
    public $description_preview;
}
