<?php

namespace common\modules\product\models\dto;

/**
 * Class ReadySetDto
 * @package common\modules\product\models\dto
 */
class ReadySetDto
{
    public int $id;
    public string $name;
    public string $slug;
    public string $photo;
    public ?string $link;
    public ?string $description;
    public ?CollectionTileDto $collection;

    /** @var ReadySetItemDto[] */
    public array $items;
}
