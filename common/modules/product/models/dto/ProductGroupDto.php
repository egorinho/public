<?php

namespace common\modules\product\models\dto;

class ProductGroupDto
{
    public int $id;
    public string $name;
    public string $slug;
    public ?int $productQuantity;
}