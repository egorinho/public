<?php

declare(strict_types=1);

namespace common\modules\product\models\dto;

/**
 * Class CatalogDataDto
 * @package common\modules\product\models\dto
 */
class CatalogDataDto
{
    public PriceFilterDto $priceFilters;
    public CategoryTileDto $category;
    public ProductGroupDto $group;

    /** @var CategoryTileDto[] */
    public array $categoryFilters;
    /** @var CollectionTileDto[] */
    public array $collectionFilters;
    /** @var ColorTileDto[] */
    public array $colorFilters;
    /** @var FieldDto[] */
    public array $fieldFilters;
    /** @var ProductDto[] */
    public array $products;
    /** @var CategoryTileInGroupDto[] */
    public array $groupCategories;
    /** @var  */
    public array $recommendedCollections;
}
