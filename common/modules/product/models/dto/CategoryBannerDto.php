<?php


namespace common\modules\product\models\dto;


class CategoryBannerDto
{
    public string $title;
    public string $content;
    public string $image;
}
