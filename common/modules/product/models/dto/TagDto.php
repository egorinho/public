<?php


namespace common\modules\product\models\dto;


class TagDto
{
    public int $id;
    public string $name;

}
