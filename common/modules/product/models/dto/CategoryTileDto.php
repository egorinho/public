<?php

namespace common\modules\product\models\dto;

/**
 * Class CategoryTileDto
 * @package common\modules\product\models\dto
 * @property string $name
 * @property string $content
 * @property string $slug
 * @property int $productQuantity
 */
class CategoryTileDto
{
    public int $id;
    public string $name;
    public ?string $content;
    public string $slug;
    public string $img;
    public ?int $productQuantity;
    public ?CategoryTileDto $parent;
    public ?CategoryArticleLinkDto $articleLink;
}
