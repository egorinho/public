<?php

namespace common\modules\product\models\dto;

/**
 * Class ReadySetItemDto
 * @package common\modules\product\models\dto
 */
class ReadySetItemDto
{
    public int $id;
    public int $width;
    public int $height;
    public int $productId;
    public string $productName;
    public string $productSlug;
    public float $rrc;
    public ?float $arc;
}
