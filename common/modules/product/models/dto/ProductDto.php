<?php

namespace common\modules\product\models\dto;

use common\modules\product\models\db\Product;

/**
 * Class ProductDto
 * @package common\modules\product\models\dto
 */
class ProductDto
{
    public $id;
    public $slug;
    public $name;
    public $article;
    public $isStock;
    public CategoryTileDto $category;
    public CollectionTileDto $collection;
    public $photoUrl;
    public $rrc;
    public $arc;
    public $discount;
    public $siteName;
    public $commonDescription;
    public $uniqDescription;
    public $comment;
    public $color;
    public $tags;
    public $pickings;
    public array $fields;
    public array $photos;
    public array $documents;
    /** @var Product[] */
    public array $sameWithDifferentColor;
}
