<?php

namespace common\modules\product\models\db;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "compilation".
 *
 * @property int $id
 * @property string $name
 * @property int|null $max_price
 * @property int|null $min_price
 * @property string|null $product_group
 * @property string|null $category
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Category $categoryModel
 * @property CompilationCollection[] $compilationCollections
 * @property Collection[] $collections
 * @property CompilationColor[] $compilationColors
 * @property Color[] $colors
 * @property CompilationFieldValue[] $compilationFieldValues
 * @property FieldValue[] $fieldValues
 */
class Compilation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compilation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['max_price', 'min_price'], 'default', 'value' => null],
            [['max_price', 'min_price'], 'integer'],
            [['product_group'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'category'], 'string', 'max' => 255],
            [
                ['category'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Category::class,
                'targetAttribute' => ['category' => 'slug']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'max_price' => 'Max Price',
            'min_price' => 'Min Price',
            'product_group' => 'Product Group',
            'category' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Category0]].
     *
     * @return ActiveQuery
     */
    public function getCategoryModel()
    {
        return $this->hasOne(Category::class, ['slug' => 'category']);
    }

    /**
     * Gets query for [[CompilationCollections]].
     *
     * @return ActiveQuery
     */
    public function getCompilationCollections()
    {
        return $this->hasMany(CompilationCollection::class, ['compilation_id' => 'id']);
    }

    /**
     * Gets query for [[Collections]].
     *
     * @return ActiveQuery
     */
    public function getCollections()
    {
        return $this->hasMany(Collection::class, ['slug' => 'collection'])->viaTable('compilation_collection', ['compilation_id' => 'id']);
    }

    /**
     * Gets query for [[CompilationColors]].
     *
     * @return ActiveQuery
     */
    public function getCompilationColors()
    {
        return $this->hasMany(CompilationColor::class, ['compilation_id' => 'id']);
    }

    /**
     * Gets query for [[Colors]].
     *
     * @return ActiveQuery
     */
    public function getColors()
    {
        return $this->hasMany(Color::class, ['slug' => 'color'])->viaTable('compilation_color', ['compilation_id' => 'id']);
    }

    /**
     * Gets query for [[CompilationFieldValues]].
     *
     * @return ActiveQuery
     */
    public function getCompilationFieldValues()
    {
        return $this->hasMany(CompilationFieldValue::class, ['compilation_id' => 'id']);
    }

    /**
     * Gets query for [[FieldValues]].
     *
     * @return ActiveQuery
     */
    public function getFieldValues()
    {
        return $this->hasMany(FieldValue::class, ['id' => 'field_value'])->viaTable('compilation_field_value', ['compilation_id' => 'id']);
    }
}
