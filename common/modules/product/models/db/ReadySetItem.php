<?php

namespace common\modules\product\models\db;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ready_set_item".
 *
 * @property int $id
 * @property int $ready_set_id
 * @property int $width
 * @property int $height
 * @property int $product_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $product
 * @property ReadySet $readySet
 */
class ReadySetItem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ready_set_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ready_set_id', 'width', 'height', 'product_id'], 'default', 'value' => null],
            [['ready_set_id', 'width', 'height', 'product_id'], 'integer'],
            [['width', 'height', 'product_id', 'ready_set_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['product_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Product::class,
                'targetAttribute' => ['product_id' => 'id']
            ],
            [
                ['ready_set_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ReadySet::class,
                'targetAttribute' => ['ready_set_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ready_set_id' => 'Ready Set ID',
            'width' => 'Width',
            'height' => 'Height',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ReadySet]].
     *
     * @return ActiveQuery
     */
    public function getReadySet()
    {
        return $this->hasOne(ReadySet::class, ['id' => 'ready_set_id']);
    }
}
