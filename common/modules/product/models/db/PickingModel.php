<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "picking_model".
 *
 * @property int|null $model_id
 * @property string|null $model
 * @property int|null $picking_id
 * @property int|null $sort
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Picking $picking
 */
class PickingModel extends \yii\db\ActiveRecord
{
    const MODEL_CATEGORY = 'category';
    const MODEL_PRODUCT = 'product';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picking_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'picking_id', 'sort'], 'default', 'value' => null],
            [['model_id', 'picking_id', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['model'], 'string', 'max' => 255],
            [['picking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Picking::className(), 'targetAttribute' => ['picking_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'model' => 'Model',
            'picking_id' => 'Picking ID',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Picking]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPicking()
    {
        return $this->hasOne(Picking::className(), ['id' => 'picking_id']);
    }
}
