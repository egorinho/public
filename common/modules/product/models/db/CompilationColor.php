<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "compilation_color".
 *
 * @property int $compilation_id
 * @property string $color
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Color $color0
 * @property Compilation $compilation
 */
class CompilationColor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compilation_color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['compilation_id', 'color', 'created_at', 'updated_at'], 'required'],
            [['compilation_id'], 'default', 'value' => null],
            [['compilation_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['color'], 'string', 'max' => 255],
            [['compilation_id', 'color'], 'unique', 'targetAttribute' => ['compilation_id', 'color']],
            [['color'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color' => 'slug']],
            [['compilation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compilation::className(), 'targetAttribute' => ['compilation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'compilation_id' => 'Compilation ID',
            'color' => 'Color',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Color0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColor0()
    {
        return $this->hasOne(Color::className(), ['slug' => 'color']);
    }

    /**
     * Gets query for [[Compilation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompilation()
    {
        return $this->hasOne(Compilation::className(), ['id' => 'compilation_id']);
    }
}
