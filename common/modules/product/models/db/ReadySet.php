<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use common\behaviors\UploadFileBehavior;
use common\helpers\FileHelper;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ready_set".
 *
 * @property int $id
 * @property int|null $collection_id
 * @property string $name
 * @property string $slug
 * @property string $photo
 * @property string $link
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property bool $home_page
 *
 * @property Collection $collection
 * @property ReadySetItem[] $readySetItems
 */
class ReadySet extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ready_set';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id'], 'default', 'value' => null],
            [['collection_id'], 'integer'],
            [['name'], 'required'],
            [['name', 'slug', 'link', 'description'], 'string'],
            [['created_at', 'updated_at', 'photo'], 'safe'],
            [['slug'], 'unique'],
            [['home_page'], 'boolean'],
            [
                ['collection_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Collection::class,
                'targetAttribute' => ['collection_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'collection_id' => 'Коллекция',
            'name' => 'Название',
            'slug' => 'ЧПУ',
            'photo' => 'Фото',
            'home_page' => 'Отображать на главной',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'link' => 'Ссылка',
            'description' => 'Описание',
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'photo'
            ],
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * Gets query for [[Collection]].
     *
     * @return ActiveQuery
     */
    public function getCollection(): ActiveQuery
    {
        return $this->hasOne(Collection::class, ['id' => 'collection_id']);
    }

    /**
     * Gets query for [[ReadySetItems]].
     *
     * @return ActiveQuery
     */
    public function getReadySetItems(): ActiveQuery
    {
        return $this->hasMany(ReadySetItem::class, ['ready_set_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getImgUrl(): string
    {
        return FileHelper::getFileUrl($this->photo);
    }
}
