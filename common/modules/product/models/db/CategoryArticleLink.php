<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "category_article_link".
 *
 * @property int $id
 * @property string|null $image
 * @property string|null $name
 * @property int|null $link
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Category[] $categories
 */
class CategoryArticleLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_article_link';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'image'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'default', 'value' => null],
            [['link'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'name' => 'Название',
            'link' => 'Ссылка',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['article_link_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getImgWebPath(): string
    {
        if (empty($this->image)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->image;
    }
}
