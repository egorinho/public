<?php

namespace common\modules\product\models\db;

use common\behaviors\CategoryBlockBehavior;
use Yii;

/**
 * This is the model class for table "category_block_type".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string|null $link
 * @property int|null $sort
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CategoryBlockTypeItem[] $typeItems
 */
class CategoryBlockType extends \yii\db\ActiveRecord
{
    public $items;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_block_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id', 'sort'], 'default', 'value' => null],
            [['category_id', 'sort'], 'integer'],
            [['created_at', 'updated_at', 'items'], 'safe'],
            [['name', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Название',
            'link' => 'Ссылка',
            'sort' => 'Сортировка',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'items' => "Подтипы",
        ];
    }

    public function getTypeItems()
    {
        return $this->hasMany(CategoryBlockTypeItem::class, ['category_block_type_id' => 'id'])->orderBy('category_block_type_item.sort ASC');
    }

    public function behaviors()
    {
        return [
            CategoryBlockBehavior::className()
        ];
    }
}
