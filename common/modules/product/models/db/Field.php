<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "field".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $type_filter
 * @property string|null $type
 * @property string|null $hint
 * @property int $sort
 * @property int $created_at
 * @property int $updated_at
 *
 * @property FieldValue[] $fieldValues
 */
class Field extends ActiveRecord
{
    /** Для синхронизации */
    public $value;

    public const TYPE_ATTRIBUTE = 'attribute';
    public const TYPE_LOGISTIC = 'logistic';
    public const TYPE_OTHER = 'other';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'field';
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['name', 'type', 'value', 'slug', 'type_filter'], 'string', 'max' => 255],
            [['sort'], 'integer'],
            [['hint'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'type' => 'Тип',
            'hint' => 'Подсказка',
            'sort' => 'Сортировка',
            'type_filter' => 'Тип фильтрации',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[FieldValues]].
     *
     * @return ActiveQuery
     */
    public function getFieldValues(): ActiveQuery
    {
        return $this->hasMany(FieldValue::class, ['field_id' => 'id']);
    }

    public static function getTypes()
    {
        return [
            static::TYPE_ATTRIBUTE => 'Атрибут',
            static::TYPE_LOGISTIC => 'Логистик',
            static::TYPE_OTHER => 'Другое'
        ];
    }
}
