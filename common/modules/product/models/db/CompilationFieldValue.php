<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "compilation_field_value".
 *
 * @property int $compilation_id
 * @property int $field_value
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Compilation $compilation
 * @property FieldValue $fieldValue
 */
class CompilationFieldValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compilation_field_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['compilation_id', 'field_value', 'created_at', 'updated_at'], 'required'],
            [['compilation_id', 'field_value'], 'default', 'value' => null],
            [['compilation_id', 'field_value'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['compilation_id', 'field_value'], 'unique', 'targetAttribute' => ['compilation_id', 'field_value']],
            [['compilation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compilation::className(), 'targetAttribute' => ['compilation_id' => 'id']],
            [['field_value'], 'exist', 'skipOnError' => true, 'targetClass' => FieldValue::className(), 'targetAttribute' => ['field_value' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'compilation_id' => 'Compilation ID',
            'field_value' => 'Field Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Compilation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompilation()
    {
        return $this->hasOne(Compilation::className(), ['id' => 'compilation_id']);
    }

    /**
     * Gets query for [[FieldValue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFieldValue()
    {
        return $this->hasOne(FieldValue::className(), ['id' => 'field_value']);
    }
}
