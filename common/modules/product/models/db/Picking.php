<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "picking".
 *
 * @property int $id
 * @property string|null $image
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PickingModel[] $pickingModels
 */
class Picking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[PickingModels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPickingModels()
    {
        return $this->hasMany(PickingModel::className(), ['picking_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'image'
            ],
        ];
    }

    /**
     * @return string
     */
    public function getImgWebPath(): string
    {
        if (empty($this->image)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->image;
    }
}
