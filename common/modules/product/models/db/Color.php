<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use common\behaviors\UploadFileBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "color".
 *
 * @property string $slug
 * @property string $name
 * @property string|null $rgb
 * @property string|null $img
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product[] $products
 */
class Color extends ActiveRecord
{
    public $product_count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'img'], 'safe'],
            [['slug', 'name'], 'string', 'max' => 255],
            [['rgb'], 'string', 'max' => 6],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'slug' => 'Slug',
            'name' => 'Название',
            'rgb' => 'RGB',
            'img' => 'Изображение',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ],
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'img'
            ],
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['color' => 'slug']);
    }

    /**
     * @return string
     */
    public function getImgWebPath(): string
    {
        if (empty($this->img)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->img;
    }
}
