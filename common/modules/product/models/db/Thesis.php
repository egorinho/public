<?php

namespace common\modules\product\models\db;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "thesis".
 *
 * @property int $id
 * @property string $name
 * @property string|null $text
 * @property int $order
 * @property string|null $img
 * @property string $created_at
 * @property string $updated_at
 */
class Thesis extends ActiveRecord
{
    public const URL_ICON_THESIS = 'thesis_icons';

    public const TYPE_HOME = 'home';
    public const TYPE_CATALOG = 'catalog';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'thesis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['text'], 'string'],
            [['order'], 'default', 'value' => 1000],
            [['order'], 'integer'],
            [['created_at', 'updated_at', 'img'], 'safe'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Содержимое',
            'order' => 'Сортировка',
            'img' => 'Иконка',
            'type' => 'Где показываем',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return string[]
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_CATALOG => 'В категории',
            self::TYPE_HOME => 'На главной'
        ];
    }
}
