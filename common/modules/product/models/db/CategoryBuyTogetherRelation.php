<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "category_buy_together_relation".
 *
 * @property int $category_one_id
 * @property int $category_two_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Category $categoryOne
 * @property Category $categoryTwo
 */
class CategoryBuyTogetherRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_buy_together_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_one_id', 'category_two_id', 'created_at', 'updated_at'], 'required'],
            [['category_one_id', 'category_two_id'], 'default', 'value' => null],
            [['category_one_id', 'category_two_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['category_one_id', 'category_two_id'], 'unique', 'targetAttribute' => ['category_one_id', 'category_two_id']],
            [['category_one_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_one_id' => 'id']],
            [['category_two_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_two_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_one_id' => 'Category One ID',
            'category_two_id' => 'Category Two ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[CategoryOne]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryOne()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_one_id']);
    }

    /**
     * Gets query for [[CategoryTwo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryTwo()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_two_id']);
    }
}
