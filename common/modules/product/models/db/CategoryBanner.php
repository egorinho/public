<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use Yii;

/**
 * This is the model class for table "category_banner".
 *
 * @property int $id
 * @property string $image
 * @property string $title
 * @property string|null $content
 * @property string $created_at
 * @property string $updated_at
 */
class CategoryBanner extends \yii\db\ActiveRecord
{
    const MAIN_ID = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'title' => 'Заголовок',
            'content' => 'Содержимое',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'image'
            ],
        ];
    }

    /**
     * @return string
     */
    public function getImgWebPath(): string
    {
        if (empty($this->image)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->image;
    }
}
