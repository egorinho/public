<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use common\behaviors\UploadFileBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "collection".
 *
 * @property int $id
 * @property int $order
 * @property string $name
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $description_preview
 * @property string $description_all
 * @property string|null $img
 * @property bool $catalog
 * @property bool $home
 *
 * @property Product[] $products
 */
class Collection extends ActiveRecord
{
    public $product_count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ],
            UploadFileBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug', 'description_preview', 'description_all', 'img'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['order'], 'integer'],
            [['catalog', 'home'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'ЧПУ',
            'created_at' => 'Создана',
            'updated_at' => 'Отредактирована',
            'description_preview' => 'Описание(превью)',
            'description_all' => 'Описание',
            'img' => 'Изображение',
            'order' => 'Сортировка',
            'catalog' => 'Показывать в каталоге',
            'home' => 'Показывать на главной'
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['collection_id' => 'id']);
    }
}
