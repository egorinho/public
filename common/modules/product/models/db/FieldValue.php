<?php

namespace common\modules\product\models\db;

use common\behaviors\SaveHintSameValueBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "field_value".
 *
 * @property int $id
 * @property int $product_id
 * @property int $field_id
 * @property string $value
 * @property string|null $hint
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Field $field
 * @property Product $product
 */
class FieldValue extends ActiveRecord
{
    public bool $saveAdmin = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'field_value';
    }

    public function behaviors()
    {
        return [
            SaveHintSameValueBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'field_id', 'value'], 'required'],
            [['product_id', 'field_id', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['product_id', 'field_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['hint'], 'string'],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => Field::class, 'targetAttribute' => ['field_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Товар',
            'field_id' => 'Аттрибут',
            'value' => 'Значение',
            'hint' => 'Подсказка',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Field]].
     *
     * @return ActiveQuery
     */
    public function getField(): ActiveQuery
    {
        return $this->hasOne(Field::class, ['id' => 'field_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return ActiveQuery
     */
    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}
