<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "compilation_collection".
 *
 * @property int $compilation_id
 * @property string $collection
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Collection $collection0
 * @property Compilation $compilation
 */
class CompilationCollection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compilation_collection';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['compilation_id', 'collection', 'created_at', 'updated_at'], 'required'],
            [['compilation_id'], 'default', 'value' => null],
            [['compilation_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['collection'], 'string', 'max' => 255],
            [['compilation_id', 'collection'], 'unique', 'targetAttribute' => ['compilation_id', 'collection']],
            [['collection'], 'exist', 'skipOnError' => true, 'targetClass' => Collection::className(), 'targetAttribute' => ['collection' => 'slug']],
            [['compilation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Compilation::className(), 'targetAttribute' => ['compilation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'compilation_id' => 'Compilation ID',
            'collection' => 'Collection',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Collection0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCollection0()
    {
        return $this->hasOne(Collection::className(), ['slug' => 'collection']);
    }

    /**
     * Gets query for [[Compilation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompilation()
    {
        return $this->hasOne(Compilation::className(), ['id' => 'compilation_id']);
    }
}
