<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_group".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $img
 * @property string $created_at
 * @property string $updated_at
 */
class ProductGroup extends ActiveRecord
{
    public $categories;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['img'], 'safe'],
            [['categories'], 'each', 'rule' => ['in', 'range' => Category::find()->column()]],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'img'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img' => 'Img',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        Category::updateAll([$this->slug => false]);
        Category::updateAll([$this->slug => true], ['id' => $this->categories]);

        return parent::beforeSave($insert);
    }
}
