<?php

namespace common\modules\product\models\db;

use Yii;

/**
 * This is the model class for table "category_block_type_item".
 *
 * @property int $id
 * @property int $category_block_type_id
 * @property string $name
 * @property string|null $link
 * @property int|null $sort
 * @property string $created_at
 * @property string $updated_at
 */
class CategoryBlockTypeItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_block_type_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_block_type_id', 'name'], 'required'],
            [['category_block_type_id', 'sort'], 'default', 'value' => null],
            [['category_block_type_id', 'sort'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_block_type_id' => 'Category Block Type ID',
            'name' => 'Name',
            'link' => 'Link',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
