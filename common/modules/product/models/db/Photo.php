<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use common\helpers\FileHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "photos".
 *
 * @property int $id
 * @property int $model_id
 * @property string $model
 * @property string $file
 * @property int $sort
 * @property string $file_url
 * @property int $is_api
 */
class Photo extends ActiveRecord
{
    public const MODEL_PRODUCT = 'product';
    public const MODEL_CATEGORY = 'category';
    public const MODEL_COLLECTION = 'collection';

    public const IS_API = 1;
    public const NOT_API = 0;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['model_id', 'sort', 'id', 'is_api'], 'integer'],
            [['file_url'], 'string'],
            [['file'], 'safe'],
            ['is_api', 'in', 'range' => [self::IS_API, self::NOT_API]],
            ['model', 'in', 'range' => [self::MODEL_CATEGORY, self::MODEL_COLLECTION, self::MODEL_PRODUCT]],
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'file'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'photos';
    }

    /**
     * @return string
     */
    public function getImgUrl(): string
    {
        if (!empty($this->file)) {
            return FileHelper::getFileUrl($this->file);
        } elseif (!empty($this->file_url)) {
            return $this->file_url;
        }

        return '';
    }

    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'id' => $this->id,
            'url' => $this->getImgUrl(),
            'order' => $this->sort,
        ];
    }
}
