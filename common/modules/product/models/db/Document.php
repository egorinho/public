<?php

namespace common\modules\product\models\db;

use common\behaviors\UploadFileBehavior;
use common\helpers\FileHelper;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $model_id
 * @property string $model
 * @property string $file
 * @property string $file_url
 * @property string $file_category
 */
class Document extends ActiveRecord
{
    public const MODEL_PRODUCT = 'product';

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['model_id'], 'integer'],
            [['model', 'file_category', 'file_url'], 'string'],
            [['file'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'documents';
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'file',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        if (!empty($this->file)) {
            return FileHelper::getFileUrl($this->file);
        } elseif (!empty($this->file_url)) {
            return $this->file_url;
        }

        return '';
    }

    /**
     * @return array
     */
    public function map(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->file,
            'order' => 1,
            'url' => $this->getUrl()
        ];
    }
}
