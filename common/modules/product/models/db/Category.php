<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use common\behaviors\RootCategoryBehavior;
use common\behaviors\UploadFileBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property int $root_category_id
 * @property string $slug
 * @property int|null $parent_id
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $img
 * @property string|null $icon
 * @property int $order
 * @property string $name_1c
 * @property bool $toilet
 * @property bool $bathroom
 * @property bool $kitchen
 * @property string $thesis
 * @property string $content
 * @property int $article_link_id
 * @property bool $is_novelty
 * @property bool $is_profitable
 * @property bool $is_sales_leaders
 * @property bool $show_leaders_home
 * @property bool $show_profitable_home
 * @property bool $show_novelty_home
 *
 * @property Category $parent
 * @property Category[] $children
 * @property Category[] $buyTogetherCategories
 * @property Field[] $fields
 * @property Product[] $products
 * @property Picking[] $pickings
 * @property Product[] $leadersProducts
 * @property Product[] $profitableProducts
 * @property Product[] $noveltyProducts
 */
class Category extends ActiveRecord
{
    public $fullname_1c;
    public $product_count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ],
            [
                'class' => UploadFileBehavior::class,
                'attribute' => 'icon'
            ],
            UploadFileBehavior::class,
            RootCategoryBehavior::class,
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'pickings' => [
                        'extraColumns' => function ($model) {
                            return [
                                'model' => PickingModel::MODEL_CATEGORY
                            ];
                        }
                    ],
                    'buyTogetherCategories'
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent_id'], 'default', 'value' => null],
            [['order'], 'default', 'value' => 1000],
            [['parent_id', 'order', 'article_link_id', 'root_category_id'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at', 'thesis', 'pickings', 'buyTogetherCategories'], 'safe'],
            [['name', 'slug', 'img', 'name_1c', 'icon'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [
                [
                    'toilet',
                    'kitchen',
                    'bathroom',
                    'is_sales_leaders',
                    'is_profitable',
                    'is_novelty',
                    'show_leaders_home',
                    'show_profitable_home',
                    'show_novelty_home',
                ],
                'boolean'
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Category::class,
                'targetAttribute' => ['parent_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'ЧПУ',
            'parent_id' => 'Родительская категория',
            'created_at' => 'Создана',
            'updated_at' => 'Отредактирована',
            'img' => 'Изображение',
            'order' => 'Сортировка',
            'thesis' => 'Тезисы',
            'content' => 'Содержимое',
            'article_link_id' => 'Блок "Ссылка на информационную статью"',
            'is_novelty' => 'Новинка',
            'is_profitable' => 'Самые выгодные',
            'is_sales_leaders' => 'Лидеры продаж',
            'show_leaders_home' => 'Показывать лидеров на главной',
            'show_profitable_home' => 'Показывать самые выгодные на главной',
            'show_novelty_home' => 'Показывать новинки на главной',
        ];
    }

    public function beforeSave($insert)
    {
        if (is_array($this->thesis)) {
            $this->thesis = implode(',', $this->thesis);
        }
        return parent::beforeSave($insert);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::class, ['id' => 'parent_id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Category::class, ['parent_id' => 'id']);
    }


    public function getPickings()
    {
        return $this->hasMany(Picking::class, ['id' => 'picking_id'])
            ->viaTable('picking_model', ['model_id' => 'id'],
                function($query) {
                    $query->onCondition(['model' => PickingModel::MODEL_CATEGORY]);
                });
    }

    /**
     * Gets query for [[Fields]].
     *
     * @return ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Field::class, ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getLeadersProducts(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['category_id' => 'id'])
            ->andWhere(['product.is_sales_leaders' => true]);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getNoveltyProducts(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['category_id' => 'id'])
            ->andWhere(['product.novelty' => true]);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return ActiveQuery
     */
    public function getProfitableProducts(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['category_id' => 'id'])
            ->andWhere(['not', ['product.arc' => 0]])
            ->andWhere(['not', ['product.arc' => null]]);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getBuyTogetherCategories(): ActiveQuery
    {
        return $this->hasMany(Category::class, ['id' => 'category_two_id'])
            ->viaTable('category_buy_together_relation', ['category_one_id' => 'id']);
    }


    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getArticleLink()
    {
        return $this->hasOne(CategoryArticleLink::class, ['id' => 'article_link_id']);
    }

    /**
     * @return string
     */
    public function getImgWebPath(): string
    {
        if (empty($this->img)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->img;
    }

    /**
     * @return string
     */
    public function getIconWebPath(): string
    {
        if (empty($this->icon)) {
            return '';
        }

        return \Yii::$app->params['apiUrl'] . $this->icon;
    }

    /**
     * @return false|int|string|null
     */
    public function getProductCount()
    {
        return Product::find()
            ->select('COUNT(id)')
            ->where(['category_id' => $this->id])
            ->orWhere(['category_id' => self::find()->where(['parent_id' => $this->id])->column()])
            ->scalar();
    }

    public static function getQueryChildrenByParams(string $paramName, string $paramValue): Query
    {
        $initialQuery = Category::find()
            ->select(['id'])
            ->where([$paramName => $paramValue]);
        $recursiveQuery = Category::find()
            ->select(['category.id'])
            ->innerJoin('r', 'r.id = category.parent_id');

        return (new Query())
            ->select(['id'])
            ->from('r')
            ->withQuery($initialQuery->union($recursiveQuery), 'r', true);
    }
}
