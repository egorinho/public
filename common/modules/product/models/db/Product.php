<?php

namespace common\modules\product\models\db;

use common\behaviors\CyrillicSlugBehavior;
use common\behaviors\MainPhotoUploadBehavior;
use common\modules\file\models\db\File;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $article
 * @property string $name
 * @property string $slug
 * @property int $category_id
 * @property int $collection_id
 * @property int $main_photo_id
 * @property float $rrc
 * @property float|null $arc
 * @property string|null $ean13
 * @property string|null $status
 * @property int|null $planned_arrival_date
 * @property string|null $site_name
 * @property string|null $common_description
 * @property string|null $uniq_description
 * @property string|null $comment
 * @property float|null $length
 * @property float|null $width
 * @property float|null $height
 * @property string|null $color
 * @property string|null $style
 * @property string|null $production_country
 * @property int|null $warranty_period
 * @property string|null $link
 * @property string|null $guid
 * @property string|null code_1c
 * @property int $created_at
 * @property int $updated_at
 * @property int $quantity
 * @property bool $is_sales_leaders
 * @property bool $novelty
 *
 * @property FieldValue[] $fieldValues
 * @property Field[] $fields
 * @property FieldValue[] $mainFieldsValues
 * @property Tag[] $tags
 * @property File[] $files
 * @property Category $category
 * @property Collection $collection
 * @property Color $colorModel
 * @property Photo[] $photos
 * @property Photo[] $apiPhotos
 * @property Photo $mainPhoto
 * @property Document[] $documents
 * @property Picking[] $pickings
 * @property Product[] $sameWithDifferentColor
 */
class Product extends ActiveRecord
{
    public const STATUS_NEW = 'Новинка';
    public const STATUS_REGULAR = 'Регулярный';
    public const STATUS_ROTATION = 'Ротация';
    public const STATUS_STOP = 'STOP дистрибуция';
    public const STATUS_OUTLET = 'OUTLET';
    public const PHOTO_MODEL_NAME = 'product';
    public const COUNT_MAIN_FIELD = 3;
    public const PERCENT_PRICE_FOR_SAME_PRODUCT = 20;

    public array $tempFields;
    public $sameWithDifferentColorIds;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return string[]
     */
    public static function statuses(): array
    {
        return [
            self::STATUS_OUTLET,
            self::STATUS_STOP,
            self::STATUS_NEW,
            self::STATUS_REGULAR,
            self::STATUS_ROTATION
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => CyrillicSlugBehavior::class,
                'attribute' => 'name'
            ],
            [
                'class' => MainPhotoUploadBehavior::class,
                'attribute' => 'main_photo_id'
            ],
            [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'tags',
                    'fields' => [
                        'extraColumns' => function ($model) {
                            return [
                                'value' => ((strlen($model->value) > 250) ? StringHelper::truncate($model->value,
                                    150) : $model->value)
                            ];
                        }
                    ],
                    'pickings' => [
                        'extraColumns' => function ($model) {
                            return [
                                'model' => PickingModel::MODEL_PRODUCT
                            ];
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['article', 'name', 'category_id', 'collection_id', 'rrc'],
                'required'
            ],
            [['is_sales_leaders', 'novelty'], 'boolean'],
            [
                [
                    'category_id',
                    'collection_id',
                    'planned_arrival_date',
                    'warranty_period',
                    'created_at',
                    'updated_at',
                    'color'
                ],
                'default',
                'value' => null
            ],
            [
                ['category_id', 'collection_id', 'planned_arrival_date', 'warranty_period'],
                'integer'
            ],
            [
                ['rrc', 'arc', 'length', 'width', 'height'],
                'number'
            ],
            [
                [
                    'article',
                    'name',
                    'photo',
                    'ean13',
                    'status',
                    'site_name',
                    'style',
                    'production_country',
                    'link',
                    'slug',
                    'color',
                ],
                'string',
                'max' => 255
            ],
            [
                ['common_description', 'uniq_description', 'comment'],
                'string',
            ],
            [
                ['article', 'slug'],
                'unique'
            ],
            [
                ['created_at', 'updated_at', 'main_photo_id', 'sameWithDifferentColorIds', 'tags', 'pickings'],
                'safe'
            ],
            [
                ['category_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Category::class,
                'targetAttribute' => ['category_id' => 'id']
            ],
            [
                ['collection_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Collection::class,
                'targetAttribute' => ['collection_id' => 'id']
            ],
            [
                ['color'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Color::class,
                'targetAttribute' => ['color' => 'slug']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Артикул',
            'name' => 'Название',
            'category_id' => 'Категория',
            'collection_id' => 'Коллекция',
            'rrc' => 'Рекомендованная розничная цена',
            'arc' => 'Аутлет розничная цена',
            'photo' => 'Фото',
            'ean13' => 'Штрих код (EAN13)',
            'status' => 'Статус',
            'planned_arrival_date' => 'Плановая дата поставки',
            'site_name' => 'Наименование для сайта',
            'common_description' => 'Общее описание',
            'uniq_description' => 'Уникальное описание',
            'comment' => 'Комментарий',
            'length' => 'Длина',
            'width' => 'Ширина',
            'height' => 'Высота',
            'color' => 'Цвет',
            'style' => 'Стиль',
            'production_country' => 'Страна производитель',
            'warranty_period' => 'Гарантийный срок',
            'link' => 'Ссылка на сайт',
            'code_1c' => 'Код в 1С',
            'guid' => 'GUID',
            'created_at' => 'Создано',
            'updated_at' => 'Отредактировано',
            'is_sales_leaders' => 'Лидеры продаж',
            'novelty' => 'Новинка'
        ];
    }

    /**
     * Gets query for [[FieldValues]].
     *
     * @return ActiveQuery
     */
    public function getFieldValues(): ActiveQuery
    {
        return $this->hasMany(FieldValue::class, ['product_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws
     */
    public function getFields(): ActiveQuery
    {
        return $this->hasMany(Field::class, ['id' => 'field_id'])
            ->viaTable('field_value', ['product_id' => 'id']);
    }


    /**
     * @return ActiveQuery
     * @throws
     */
    public function getMainFieldsValues(): ActiveQuery
    {
        return $this->hasMany(FieldValue::class, ['product_id' => 'id'])
            ->leftJoin(Field::tableName(), 'field_value.field_id = field.id')
            ->orderBy('field.sort ASC')
            ->limit(static::COUNT_MAIN_FIELD);
    }

    /**
     * Gets query for [[Files]].
     *
     * @return ActiveQuery
     */
    public function getFiles(): ActiveQuery
    {
        return $this->hasMany(File::class, ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return ActiveQuery
     */
    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Collection]].
     *
     * @return ActiveQuery
     */
    public function getCollection(): ActiveQuery
    {
        return $this->hasOne(Collection::class, ['id' => 'collection_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getColorModel(): ActiveQuery
    {
        return $this->hasOne(Color::class, ['slug' => 'color']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['model_id' => 'id'])
            ->andOnCondition(['model' => Photo::MODEL_PRODUCT])
            ->orderBy('sort, id');
    }

    /**
     * @return ActiveQuery
     */
    public function getApiPhotos(): ActiveQuery
    {
        return $this->hasMany(Photo::class, ['model_id' => 'id'])
            ->andOnCondition(['model' => Photo::MODEL_PRODUCT, 'is_api' => Photo::IS_API])
            ->indexBy('file_url')
            ->orderBy('sort, id');
    }

    /**
     * @return ActiveQuery
     */
    public function getMainPhoto(): ActiveQuery
    {
        return $this->hasOne(Photo::class, ['id' => 'main_photo_id']);
    }

    /**
     * @return ActiveQuery
     * @throws
     */
    public function getTags(): ActiveQuery
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])
            ->indexBy('name_1c')
            ->viaTable('product_tag', ['product_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments(): ActiveQuery
    {
        return $this->hasMany(Document::class, ['model_id' => 'id'])
            ->andOnCondition(['model' => Document::MODEL_PRODUCT]);
    }

    public function getPickings()
    {
        return $this->hasMany(Picking::class, ['id' => 'picking_id'])
            ->viaTable('picking_model', ['model_id' => 'id'],
                function($query) {
                    $query->onCondition(['model' => PickingModel::MODEL_PRODUCT]);
                });
    }

    /**
     * @return Product[]
     */
    public function getSameWithDifferentColor(): array
    {
        $initialQuery = (new Query())
            ->select([
                '(CASE WHEN product_one = ' . $this->id . ' THEN product_two ELSE product_one END) AS id',
                '(' . $this->id . ') AS exclude'
            ])
            ->from(ProductColorRelation::tableName())
            ->where(['product_one' => $this->id])
            ->orWhere(['product_two' => $this->id]);
        $recursiveQuery = ProductColorRelation::find()
            ->select([
                '(CASE WHEN product_color_relation.product_one = r.id THEN product_color_relation.product_two ELSE product_color_relation.product_one END) AS id',
                'r.id AS exclude'
            ])
            ->innerJoin('r', 'product_color_relation.product_one = r.id OR product_color_relation.product_two = r.id')
            ->where('product_color_relation.product_one <> r.exclude')
            ->orWhere('product_color_relation.product_two <> r.exclude');
        $mainQuery = (new Query())
            ->select(['id'])
            ->from('r')
            ->withQuery($initialQuery->union($recursiveQuery), 'r', true);

        return Product::find()
            ->where(['id' => $mainQuery])
            ->andWhere(['not', ['id' => $this->id]])
            ->distinct()
            ->all();
    }


    public function getPickingsCategory()
    {
        $pickings = $this->pickings;

        if (empty($pickings)) {

            $queryParentIds = (new Query())
                ->select(['id'])
                ->from('r')
                ->withQuery(
                    (Category::find()
                        ->select(['id', 'parent_id'])
                        ->where(['id' => $this->category_id]))
                        ->union(
                            Category::find()
                                ->select(['category.id', 'category.parent_id'])
                                ->innerJoin('r', 'r.parent_id = category.id')),
                    'r', true);


            $pickings = Picking::find()
                ->andWhere([
                    'in',
                    'id',
                    PickingModel::find()
                        ->alias('PM1')
                        ->select('PM1.picking_id')
                        ->andWhere([
                            'in',
                            'model_id',
                            PickingModel::find()
                                ->alias('PM2')
                                ->select('PM2.model_id')
                                ->andWhere(['PM2.model' => PickingModel::MODEL_CATEGORY])
                                ->andWhere(['in', 'PM2.model_id', $queryParentIds])
                                ->orderBy([
                                    new \yii\db\Expression("array_position(array[" . implode(',',
                                            $queryParentIds->column()) . "], model_id)")
                                ])
                                ->limit(1)
                        ])
                ])->all();
        }

        return $pickings;
    }


    /**
     * @return string
     */
    public function getMainPhotoUrl(): string
    {
        if (empty($this->mainPhoto)) {
            return '';
        }

        return $this->mainPhoto->getImgUrl();
    }

    /**
     * @return array
     */
    public function getMappedImages(): array
    {
        return array_map(function (Photo $photo) {
            return $photo->map();
        }, $this->photos);
    }

    /**
     * @return array
     */
    public function getMappedDocuments(): array
    {
        return array_map(function (Document $document) {
            return $document->map();
        }, $this->documents);
    }
}
