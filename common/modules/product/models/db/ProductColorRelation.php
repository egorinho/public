<?php

declare(strict_types=1);

namespace common\modules\product\models\db;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "product_color_relation".
 *
 * @property int $product_one
 * @property int $product_two
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $productOne
 * @property Product $productTwo
 */
class ProductColorRelation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_color_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_one', 'product_two'], 'required'],
            [['product_one', 'product_two'], 'default', 'value' => null],
            [['product_one', 'product_two'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['product_one', 'product_two'], 'unique', 'targetAttribute' => ['product_one', 'product_two']],
            [['product_one'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_one' => 'id']],
            [['product_two'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_two' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_one' => 'Product One',
            'product_two' => 'Product Two',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[ProductOne]].
     *
     * @return ActiveQuery
     */
    public function getProductOne()
    {
        return $this->hasOne(Product::class, ['id' => 'product_one']);
    }

    /**
     * Gets query for [[ProductTwo]].
     *
     * @return ActiveQuery
     */
    public function getProductTwo()
    {
        return $this->hasOne(Product::class, ['id' => 'product_two']);
    }
}
