<?php

namespace common\modules\product\models\search;

use common\modules\product\mappers\FieldMapper;
use common\modules\product\models\db\Field;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\dto\FieldDto;
use yii\db\ActiveQuery;

/**
 * Class FieldValueFilterSearch
 * @package common\modules\product\models\search
 */
class FieldValueFilterSearch
{
    private ActiveQuery $query;
    private string $field;

    /**
     * FieldValueFilterSearch constructor.
     * @param ActiveQuery $query
     * @param string $field
     */
    public function __construct(ActiveQuery $query, string $field)
    {
        $this->field = $field;
        $this->query = clone $query;
    }

    /**
     * @return FieldDto
     * @throws
     */
    public function search(): FieldDto
    {

        $values = FieldValue::find()
            ->innerJoin('field', 'field.id = field_value.field_id')
            ->where([
                'field_value.product_id' => $this->query->select(['product.id']),
                'field.slug' => $this->field,
                'field.type' => Field::TYPE_ATTRIBUTE
            ])
            ->orderBy('field.sort')
            ->all();

        return FieldMapper::mapByFieldValueArray($values);
    }
}
