<?php

namespace common\modules\product\models\search;

use common\modules\product\models\db\Field;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\dto\FieldDto;
use yii\db\ActiveQuery;

/**
 * Class FieldFilterSearch
 * @package common\modules\product\models\search
 */
class FieldFilterSearch
{
    private ActiveQuery $query;
    private array $excludedFilters;

    /**
     * FieldFilterSearch constructor.
     * @param ActiveQuery $query
     * @param string[] $excludedFilters
     */
    public function __construct(ActiveQuery $query, array $excludedFilters)
    {
        $this->excludedFilters = $excludedFilters;
        $this->query = clone $query;
    }

    /**
     * @return FieldDto[]
     * @throws
     */
    public function search(): array
    {
        $values = FieldValue::find()
            ->select([
                'field_value.value as value',
                'field.slug as slug',
                'field.name as name',
                'field.type_filter as type_filter',
                'field.hint as field_hint',
                'field_value.hint as field_value_hint'
            ])
            ->innerJoin('field', 'field.id = field_value.field_id')
            ->where([
                'field_value.product_id' => $this->query->select(['product.id']),
                'field.type' => Field::TYPE_ATTRIBUTE
            ])
            ->andWhere(['not', ['field.slug' => array_keys($this->excludedFilters)]])
            ->orderBy('field.sort')
            ->asArray()
            ->all();

        $fields = [];

        foreach ($values as $value) {
            if (!isset($fields[$value['slug']])) {
                $dto = new FieldDto();
                $dto->slug = $value['slug'];
                $dto->name = $value['name'];
                $dto->hint = $value['field_hint'];
                $dto->type_filter = $value['type_filter'];
                $dto->values = [];
            } else {
                $dto = $fields[$value['slug']];
            }

            $exists = false;

            foreach ($dto->values as $existsValue) {
                if ($value['value'] === $existsValue['value']) {
                    $exists = true;
                    break;
                }
            }

            if (!$exists) {
                $dto->values[] = ['value' => $value['value'], 'hint' => $value['field_value_hint']];
            }

            $fields[$value['slug']] = $dto;
        }

        return array_values($fields);
    }
}
