<?php

namespace common\modules\product\models\search;

use common\modules\product\mappers\CollectionMapper;
use common\modules\product\models\db\Collection;
use common\modules\product\models\db\Product;
use common\modules\product\models\dto\CategoryTileDto;
use yii\db\ActiveQuery;

/**
 * Class CollectionFilterSearch
 * @package common\modules\product\models\search
 */
class CollectionFilterSearch
{
    private ActiveQuery $query;

    /**
     * CollectionFilterSearch constructor.
     * @param ActiveQuery $query
     */
    public function __construct(ActiveQuery $query)
    {
        $this->query = clone $query;
    }

    /**
     * @return CategoryTileDto[]
     */
    public function search(): array
    {
        $collections = Collection::find()
            ->select(['collection.*, COUNT(product.id) as product_count'])
            ->innerJoin(Product::tableName(), 'product.collection_id = collection.id')
            ->where(['product.id' => $this->query->select(['product.id'])])
            ->groupBy(['collection.id'])
            ->all();

        return CollectionMapper::tiles($collections);
    }
}
