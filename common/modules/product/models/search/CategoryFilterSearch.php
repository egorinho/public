<?php

declare(strict_types=1);

namespace common\modules\product\models\search;

use common\modules\product\mappers\CategoryMapper;
use common\modules\product\models\db\Category;
use common\modules\product\models\dto\CategoryTileDto;
use yii\db\ActiveQuery;

/**
 * Class CategoryFilterSearch
 * @package common\modules\product\models\search
 */
class CategoryFilterSearch
{
    private ActiveQuery $query;

    /**
     * CategoryFilterSearch constructor.
     * @param ActiveQuery $query
     */
    public function __construct(ActiveQuery $query)
    {
        $this->query = clone $query;
    }

    /**
     * @param string $exceptCategorySlug
     * @return CategoryTileDto[]
     */
    public function search(?string $exceptCategorySlug): array
    {
        $query = Category::find()
            ->select(['category.*', 'COUNT(product.id) as product_count'])
            ->leftJoin('product', 'product.category_id = category.id')
            ->where(['product.id' => $this->query->select(['product.id'])])
            ->groupBy(['category.id']);

        if ($exceptCategorySlug){
            $query->andWhere(['<>','category.slug', $exceptCategorySlug]);
        }

        return CategoryMapper::tiles($query->all());
    }
}
