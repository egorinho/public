<?php

declare(strict_types=1);

namespace common\modules\product\models\search;

use common\modules\product\models\CatalogParams;
use common\modules\product\models\db\Category;
use common\modules\product\models\db\Collection;
use common\modules\product\models\db\Field;
use common\modules\product\models\db\FieldValue;
use common\modules\product\models\db\Product;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class ProductSearch
 * @package common\modules\product\models\search
 */
class ProductQueryCreator extends Model
{
    private CatalogParams $params;
    private ActiveQuery $query;

    /**
     * ProductQueryCreator constructor.
     * @param CatalogParams $params
     * @param array $config
     */
    public function __construct(CatalogParams $params, $config = [])
    {
        $this->params = $params;

        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @return ActiveQuery
     */
    public function createQuery(): ActiveQuery
    {
        $this->query = Product::find();
        $this->searchByCategory();
        $this->searchByCollection();
        $this->searchByGroup();
        $this->query->andFilterHaving(['>=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->minPrice]);
        $this->query->andFilterHaving(['<=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->maxPrice]);
        $this->query->andFilterWhere(['product.color' => $this->params->color]);
        $this->searchByOtherParams();
        $this->query->groupBy('product.id');

        return $this->query;
    }

    /**
     * @return ActiveQuery
     */
    public function createQueryWithoutPrice(): ActiveQuery
    {
        $this->query = Product::find();
        $this->searchByCategory();
        $this->searchByCollection();
        $this->searchByGroup();
        $this->query->andFilterWhere(['product.color' => $this->params->color]);
        $this->searchByOtherParams();
        $this->query->groupBy('product.id');

        return $this->query;
    }

    /**
     * @return ActiveQuery
     */
    public function createQueryWithoutCollection(): ActiveQuery
    {
        $this->query = Product::find();
        $this->searchByCategory();
        $this->searchByGroup();
        $this->query->andFilterHaving(['>=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->minPrice]);
        $this->query->andFilterHaving(['<=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->maxPrice]);
        $this->query->andFilterWhere(['product.color' => $this->params->color]);
        $this->searchByOtherParams();
        $this->query->groupBy('product.id');

        return $this->query;
    }

    /**
     * @return ActiveQuery
     */
    public function createQueryWithoutColor(): ActiveQuery
    {
        $this->query = Product::find();
        $this->searchByCategory();
        $this->searchByCollection();
        $this->searchByGroup();
        $this->query->andFilterHaving(['>=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->minPrice]);
        $this->query->andFilterHaving(['<=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->maxPrice]);
        $this->searchByOtherParams();
        $this->query->groupBy('product.id');

        return $this->query;
    }

    /**
     * @param string $field
     * @return ActiveQuery
     */
    public function createQueryWithoutFieldParam(string $field): ActiveQuery
    {
        $this->query = Product::find();
        $this->searchByCategory();
        $this->searchByCollection();
        $this->searchByGroup();
        $this->query->andFilterHaving(['>=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->minPrice]);
        $this->query->andFilterHaving(['<=', 'COALESCE(NULLIF(arc,0),rrc)', $this->params->maxPrice]);
        $this->query->andFilterWhere(['product.color' => $this->params->color]);
        $this->searchByOtherParamsWithoutOne($field);
        $this->query->groupBy('product.id');

        return $this->query;
    }

    private function searchByOtherParams(): void
    {
        $otherParams = $this->params->getOtherParams();

        if (empty($otherParams)) {
            return;
        }

        $this->query->leftJoin(FieldValue::tableName(), 'field_value.product_id = product.id');
        $this->query->leftJoin(Field::tableName(), 'field.id = field_value.field_id');

        foreach ($otherParams as $slug => $value) {
            $this->query->andWhere([
                'product.id' => FieldValue::find()
                    ->select(['field_value.product_id'])
                    ->innerJoin('field', 'field.id = field_value.field_id')
                    ->where(['field.slug' => $slug, 'field_value.value' => $value])
            ]);
        }
    }

    private function searchByOtherParamsWithoutOne(string $field): void
    {
        $otherParams = $this->params->getOtherParams();

        if (empty($otherParams)) {
            return;
        }

        $this->query->leftJoin(FieldValue::tableName(), 'field_value.product_id = product.id');
        $this->query->leftJoin(Field::tableName(), 'field.id = field_value.field_id');

        foreach ($otherParams as $slug => $value) {
            $this->query->andWhere([
                'product.id' => FieldValue::find()
                    ->select(['field_value.product_id'])
                    ->innerJoin('field', 'field.id = field_value.field_id')
                    ->where(['field.slug' => $slug, 'field_value.value' => $value])
            ]);
        }
    }

    private function searchByCategory(): void
    {
        if (!empty($this->params->category)) {
            $initialQuery = Category::find()
                ->select(['id'])
                ->where(['slug' => $this->params->category]);
            $recursiveQuery = Category::find()
                ->select(['category.id'])
                ->innerJoin('r', 'r.id = category.parent_id');
            $mainQuery = (new Query())
                ->select(['id'])
                ->from('r')
                ->withQuery($initialQuery->union($recursiveQuery), 'r', true);

            $this->query->andWhere(['category_id' => $mainQuery]);
        }
    }

    private function searchByCollection(): void
    {
        if (!empty($this->params->collection)) {
            $this->query->andWhere([
                'product.collection_id' => Collection::find()
                    ->select(['id'])
                    ->where(['slug' => $this->params->collection])
            ]);
        }
    }

    private function searchByGroup(): void
    {
        if (!empty($this->params->group)) {
            $this->query->andWhere([
                'category_id' => Category::find()
                    ->select(['id'])
                    ->where([$this->params->group => true])
            ]);
        }
    }

    public function getCategorySlug()
    {
        return $this->params->category;
    }
}
