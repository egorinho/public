<?php

namespace common\modules\product\models\search;

use common\modules\product\models\db\Product;
use common\modules\product\models\dto\PriceFilterDto;
use yii\db\ActiveQuery;

/**
 * Class PriceFilterSearch
 * @package common\modules\product\models\search
 */
class PriceFilterSearch
{
    private ActiveQuery $query;

    /**
     * PriceFilterSearch constructor.
     * @param ActiveQuery $query
     */
    public function __construct(ActiveQuery $query)
    {
        $this->query = clone $query;
    }

    /**
     * @return PriceFilterDto
     */
    public function search(): PriceFilterDto
    {
        $limits = Product::find()
            ->select(['MIN(COALESCE(NULLIF(arc, 0),rrc)), MAX(COALESCE(NULLIF(arc, 0),rrc))'])
            ->where(['id' => $this->query->select(['product.id'])])
            ->asArray()
            ->one();

        return new PriceFilterDto($limits['min'], $limits['max']);
    }
}
