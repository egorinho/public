<?php

namespace common\modules\product\models\search;

use common\mappers\ColorMapper;
use common\modules\product\models\db\Color;
use common\modules\product\models\db\Product;
use common\modules\product\models\dto\CategoryTileDto;
use common\modules\product\models\dto\ColorTileDto;
use yii\db\ActiveQuery;

/**
 * Class ColorFilterSearch
 * @package common\modules\product\models\search
 */
class ColorFilterSearch
{
    private ActiveQuery $query;

    /**
     * ColorFilterSearch constructor.
     * @param ActiveQuery $query
     */
    public function __construct(ActiveQuery $query)
    {
        $this->query = clone $query;
    }

    /**
     * @return ColorTileDto[]
     */
    public function search(): array
    {
        $colors = Color::find()
            ->select(['color.*, COUNT(product.id) as product_count'])
            ->innerJoin(Product::tableName(), 'product.color = color.slug')
            ->where(['product.id' => $this->query->select(['product.id'])])
            ->groupBy(['color.slug'])
            ->all();

        return ColorMapper::tiles($colors);
    }
}
