<?php

namespace common\modules\product\models\search;

use common\modules\product\mappers\ProductMapper;
use common\modules\product\models\CatalogParams;
use common\modules\product\models\dto\ProductDto;
use yii\db\ActiveQuery;

/**
 * Class ProductSearch
 * @package common\modules\product\models\search
 */
class ProductSearch
{
    private CatalogParams $params;
    private ActiveQuery $query;

    /**
     * ProductSearch constructor.
     * @param ActiveQuery $query
     * @param CatalogParams $params
     * @param array $config
     */
    public function __construct(ActiveQuery $query, CatalogParams $params, $config = [])
    {
        $this->params = $params;
        $this->query = clone $query;
    }

    /**
     * @return ProductDto[]
     */
    public function search(): array
    {
        $this->query->select([
            'COALESCE(NULLIF(arc,0),rrc) as price,
            ((rrc - COALESCE(NULLIF(arc, 0), rrc)) * 100 / rrc) as discount_percent,
            product.*'
        ]);

        $this->setSort();

        if ($this->params->offset) {
            $this->query->offset($this->params->offset);
        }

        $this->query->limit(CatalogParams::LIMIT);

        return ProductMapper::mapAll($this->query->all());
    }

    private function setSort(): void
    {
        switch ($this->params->sort) {
            case CatalogParams::SORT_DISCOUNT:
                $this->query->orderBy(['discount_percent' => SORT_DESC]);
                break;
            case CatalogParams::SORT_PRICE_DESC:
                $this->query->orderBy(['price' => SORT_DESC]);
                break;
            case CatalogParams::SORT_PRICE_ASC:
                $this->query->orderBy(['price' => SORT_ASC]);
                break;
            default:
                $this->query->orderBy(['product.id' => SORT_ASC]);
        }
    }
}
