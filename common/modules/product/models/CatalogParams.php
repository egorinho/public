<?php

namespace common\modules\product\models;

use common\modules\product\models\db\Category;
use common\modules\product\models\db\ProductGroup;
use common\modules\product\validators\CollectionValidator;
use common\modules\product\validators\ColorValidator;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class CatalogParams
 * @package common\modules\product\models
 */
class CatalogParams extends Model
{
    public const LIMIT = 18;
    public const SORT_PRICE_ASC = 'priceAsc';
    public const SORT_PRICE_DESC = 'priceDesc';
    public const SORT_DISCOUNT = 'discount';

    public $maxPrice;
    public $minPrice;
    public $group;
    public $category;
    public $offset;
    public $sort;
    public $collection;
    public $color;

    private array $params;

    /**
     * CatalogParams constructor.
     * @param array $params
     * @param array $config
     */
    public function __construct(array $params, $config = [])
    {
        $this->params = $params;
        $this->checkValidate();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['minPrice', 'maxPrice'], 'number'],
            [['offset'], 'integer'],
            ['category', 'in', 'range' => Category::find()->select(['slug'])->column()],
            ['group', 'in', 'range' => ProductGroup::find()->select(['slug'])->column()],
            ['collection', CollectionValidator::class],
            ['color', ColorValidator::class],
            ['sort', 'in', 'range' => [self::SORT_DISCOUNT, self::SORT_PRICE_ASC, self::SORT_PRICE_DESC]],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return '';
    }

    /**
     * @return string[]
     */
    public function getFixedParams(): array
    {
        return ['minPrice', 'maxPrice', 'collection', 'offset', 'color', 'slug', 'sort', 'group', 'category'];
    }

    /**
     * @return array
     */
    public function getOtherParams(): array
    {
        $otherParams = [];

        foreach ($this->params as $slug => $value) {
            if (!in_array($slug, $this->getFixedParams())) {
                $otherParams[$slug] = explode(',', $value);
            }
        }

        return $otherParams;
    }

    /**
     * @throws
     */
    private function checkValidate(): void
    {
        if ($this->load($this->params)) {
            if (!$this->validate()) {
                throw new Exception('Params not valid');
            }
        }
    }
}
