<?php

namespace api\controllers;

use yii\rest\Controller;
use Throwable;

/**
 * Class ApiController
 * @package api\controllers
 */
class ApiController extends Controller
{
    /**
     * @inheritDoc
     */
    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (Throwable $exception) {
            return $exception->getMessage();
        }
    }
}
