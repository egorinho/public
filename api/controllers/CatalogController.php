<?php

namespace api\controllers;

use common\modules\product\services\CatalogService;
use Yii;

/**
 * Class CatalogController
 * @package api\controllers
 */
class CatalogController extends ApiController
{
    private CatalogService $catalogService;

    public function __construct($id, $module, CatalogService $catalogService, $config = [])
    {
        $this->catalogService = $catalogService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @OA\Get(
     *     path="/catalog",
     *     tags={"Category"},
     *     @OA\Parameter(
     *         name="category",
     *         description="category slug",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="group",
     *         description="group slug:bathroom, kitchen, toilet",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="minPrice",
     *         description="integer",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="maxPrice",
     *         description="integer",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="offset",
     *         description="integer",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="collection",
     *         description="array of collections slug separated by commas",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="array of colors slug separated by commas",
     *         description="string",
     *         in="query"
     *     ),
     *     @OA\Parameter(
     *         name="sort",
     *         description="priceAsc, priceDesc, discount",
     *         in="query"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success response",
     *     )
     * )
     * @return array
     */
    public function actionIndex()
    {
        return $this->catalogService->getData(Yii::$app->request->queryParams);
    }
}
